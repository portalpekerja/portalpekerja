<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Absen;

class AbsenController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user()->id;
        $date = date("Y-m-d", strtotime('now'));
        $cek_absen = Absen::where(['user_id' => $user, 'date' => $date])
            ->get()
            ->first();
        if (is_null($cek_absen)) {
            $info = array(
                "status" => "Anda belum melakukan absensi",
                "btnIn"  => "",
                "btnOut" => "disable"
            );
        } else if (is_null($cek_absen->time_out == NULL)) {
            $info = array(
                "status" => "Jangan lupa absen pulang",
                "btnIn"  => "disable",
                "btnOut" => ""
            );
        } else {
            $info = array(
                "status" => "Absen hari ini telah selesai",
                "btnIn"  => "disable",
                "btnOut" => "disable"
            );
        }


        $data_absen = Absen::where('user_id', $user)
            ->paginate(10);
        return view('absen.absen', compact('data_absen', 'cek_absen', 'info'));
    }
    public function doabsen(Request $request)
    {
        $user = Auth::user()->id;
        $date = date("Y-m-d", strtotime('now'));
        $time = date("H:i:s");
        $note = $request->note;

        $absen = new Absen;
        // absen masuk
        if (isset($request->btnIn)) {

            $cek_double =  $absen->where(['date' => $date, 'user_id' => $user])
                ->count();

            if ($cek_double > 0) {
                return redirect()->back();
            }

            $absen->create([
                'user_id' => $user,
                'date' => $date,
                'time_in' => $time,
                'note' => $note

            ]);

            return redirect()->back();

            // absen keluar
        } else if (isset($request->btnOut)) {
            $absen->where(['date' => $date, 'user_id' => $user])
                ->update([
                    'time_out' => $time,
                    'note' => $note

                ]);

            return redirect()->back();
        }
        return $request->all();
    }
}
