<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'clearance'])->except('index');
    }

    public function manageCompany()
    {

        $company = Company::orderby('id', 'desc')->get();

        return view('company_setting.index', compact('company'));
    }
}
