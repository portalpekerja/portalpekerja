<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Todo;
use App\Dashboard;
use App\User;
use Auth;
use Alert;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $news = Post::orderby('id', 'desc')->paginate(6, ['*'], 'news');
        $todo = Todo::orderby('id', 'desc')->where('user_id', Auth::user()->id)->paginate(5, ['*'], 'todo');
        $birth_now =  User::whereRaw("DATE_FORMAT(birthofdate, '%m-%d') = DATE_FORMAT(now(),'%m-%d')")
            ->orWhereRaw("DATE_FORMAT(birthofdate,'%m-%d') = '02-29' and DATE_FORMAT(birthofdate, '%m') = '02' AND 
        LAST_DAY(NOW()) = DATE(NOW())")
            ->get();

        Alert::success('Dont forget to attendance', 'Welcome ' . Auth::user()->name . ' !')->autoclose(4500);
        return view('dashboard.dashboard', compact('news', 'todo', 'users', 'birth_now'));
    }
}
