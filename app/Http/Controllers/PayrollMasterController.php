<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\salary_level;
use Illuminate\Support\Facades\Validator;
use App\bpjs_type;
use App\leave_type;
use Alert;
use App\incentive;
use App\PPH;
use App\PTKP;

class PayrollMasterController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'clearance'])->except('index');
    }

    // --------------------------salary level section-----------------------------------------//

    // view salary_level
    public function SalaryLevelMasterIndex()
    {

        $salary_level = salary_level::orderby('id', 'desc')->get();

        return view('master_data.PayrollMaster.SalaryLevelMaster', compact('salary_level'));
    }

    // insert salary_level
    public function SalaryLevelMasterStore(Request $request)
    {
        $salary_level = salary_level::all();

        $rules = [
            'level_name' => 'required|',
            'salary_amount' => 'required',
            'year' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("/masterdata-payrollmaster-salarylevelmaster")->withErrors($validator)->withInput();
        }

        $newSalaryLevel = new salary_level();
        $newSalaryLevel->level_name = $request['level_name'];
        $newSalaryLevel->salary_amount = $request['salary_amount'];
        $newSalaryLevel->year = $request['year'];

        $newSalaryLevel->save();
        // Alert::success('New Salary Type has beed added', 'Success Added !')->persistent("Close");
        return redirect("/masterdata-payrollmaster-salarylevelmaster")->with('success','Data has beed added.');
    }

    //  edit salary level
    public function editSalaryLevel($id)
    {

        $editSalaryLevel = salary_level::find($id);

        return view('master_data.PayrollMaster.SalaryLevelMaster', ['salary_level' => $editSalaryLevel]);
    }

    public function doeditSalaryLevel(Request $request, $id)
    {
        $rules = [
            'level_name' => 'required   ',
            'salary_amount' => 'required',
            'year' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect("/masterdata-payrollmaster-salarylevelmaster")->withErrors($validator)->withInput();
        }
        $editSalaryLevel = salary_level::find($id);
        // ini untuk narik yang udah diinput
        $editSalaryLevel->level_name = $request['level_name'];
        $editSalaryLevel->salary_amount = $request['salary_amount'];
        $editSalaryLevel->year = $request['year'];
        $editSalaryLevel->save();
        // untuk meredirect dan memberikan notif sukses
        // Alert::success('User ' . "<b>$editSalaryLevel->level_name</b>"  . ' has been updated', 'Success Edited !')->persistent("Close");
        return redirect("/masterdata-payrollmaster-salarylevelmaster")->with('success',$editSalaryLevel->level_name.' has beed edited.');;
    }

    // delete salary_level
    public function deleteSalaryLevel($id)
    {
        $salary_level = salary_level::find($id);
        $salary_level->delete();

        return redirect("/masterdata-payrollmaster-salarylevelmaster")->with('success',$salary_level->level_name. ' has beed deleted.');
    }

    // ini untuk search (masih ngaco)
    public function searchSalaryLevel(Request $request)
    {

        $salary_level = salary_level::selectRaw('salary_level.id, salary_level.level_name')
            ->whereRaw(
                'salary_level.level_name = ?',
                [$request->search]
            )
            ->paginate(15);

        $search = $request->search;

        return view('master_data.PayrollMaster.SalaryLevelMaster', compact('salary_level', 'search'));
    }

    // --------------------------salary level section-----------------------------------------//


    // --------------------------BPJS Type section-----------------------------------------//

    // view BPJS_Type
    public function BPJSTypeMasterIndex()
    {
        $bpjs_type = bpjs_type::orderby('id', 'desc')->get();
        return view('master_data.PayrollMaster.BPJSTypeMaster', compact('bpjs_type'));
    }

    // insert BPJS_Type
    public function BPJSTypeMasterStore(Request $request)
    {
        $bpjs_type = bpjs_type::all();

        $rules = [
            'BPJS_Percentage' => 'required',
            'year' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("/masterdata-payrollmaster-BPJStypemaster")->withErrors($validator)->withInput();
        }

        $newBPJSType = new bpjs_type();
        $newBPJSType->BPJS_Percentage = $request['BPJS_Percentage'];
        $newBPJSType->year = $request['year'];

        $newBPJSType->save();


        // Alert::success('BPJS Type has been added', 'Success Added !')->persistent("Close");
        return redirect("/masterdata-payrollmaster-BPJStypemaster")->with('success','New BPJS Type has been added.');
    }

    //  edit salary level
    public function editBPJSType($id)
    {

        $editBPJStype = bpjs_type::find($id);

        return view('master_data.PayrollMaster.BPJSTypeMaster', ['bpjs_type' => $editBPJStype]);
    }

    public function doeditBPJSType(Request $request, $id)
    {
        $rules = [
            'BPJS_Percentage' => 'required',
            'year' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("/masterdata-payrollmaster-BPJStypemaster")->withErrors($validator)->withInput();
        }


        $editBPJStype = bpjs_type::find($id);

        // ini untuk narik yang udah diinput
        $editBPJStype->BPJS_Percentage = $request['BPJS_Percentage'];
        $editBPJStype->year = $request['year'];
        $editBPJStype->save();
        // untuk meredirect dan memberikan notif sukses

        // Alert::success('BPJS Type has been edited', 'Success Edited !')->persistent("Close");
        return redirect("/masterdata-payrollmaster-BPJStypemaster")->with('success',' BPJS Type has been edited.');
    }

    // delete salary_level
    public function deleteBPJSType($id)
    {
        $bpjs_type = bpjs_type::find($id);
        $bpjs_type->delete();

        // Alert::success('BPJS Type has been deleted', 'Success Deleted !')->persistent("Close");
        return redirect("/masterdata-payrollmaster-BPJStypemaster")->with('success',' BPJS Type has been deleted.');
    }

    // ini untuk search (masih ngaco)
    public function searchBPJSType(Request $request)
    {

        $bpjs_type = bpjs_type::selectRaw('bpjs_type.id, bpjs_type.BPJS_Percentage')
            ->whereRaw(
                'bpjs_type.BPJS_Percentage = ?',
                [$request->search]
            )
            ->paginate(15);

        $search = $request->search;

        return view('master_data.PayrollMaster.BPJSTypeMaster', compact('bpjs_type', 'search'));
    }

    // --------------------------BPJS Type section-----------------------------------------//

    // --------------------------Leave Type section-----------------------------------------//



    public function LeaveTypeMasterIndex()
    {
        $leave_type = leave_type::orderby('id', 'desc')->get();

        return view('master_data.PayrollMaster.LeaveTypeMaster', compact('leave_type'));
    }
    // add
    public function LeaveTypeMasterStore(Request $request)
    {
        // dd('asdad');
        $leave_type = leave_type::all();
        $rules = [
            'leave_type_name' => 'required',
            'day_amount' => 'required',
            'year' => 'required'

        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("masterdata-payrollmaster-leavetypemaster")->withErrors($validator)->withInput();
        }

        $newLeaveType = new leave_type();
        $newLeaveType->leave_type_name = $request['leave_type_name'];
        $newLeaveType->day_amount = $request['day_amount'];
        $newLeaveType->year = $request['year'];
        $newLeaveType->save();

        // Alert::success('New Leave Type berhasil ditambahkan', 'Success Added !')->persistent("Close");
        return redirect("masterdata-payrollmaster-leavetypemaster")->with('success','New Incentive has beed added.');
    }
    // delete
    public function deleteLeaveType($id)
    {
        $leave_type = leave_type::find($id);
        $leave_type->delete();

        // Alert::success($leave_type->leave_type_name . ' has been Deleted', 'Success Deleted !')->persistent("Close");
        return redirect("/masterdata-payrollmaster-leavetypemaster")->with('success',$leave_type->leave_type_name . ' has been deleted.');
    }


    public function editLeaveType($id)
    {

        $leave_type = leave_type::find($id);

        return view('master_data.PayrollMaster.LeaveTypeMaster', ['leave_type' => $leave_type]);
    }

    public function doeditLeaveType(Request $request, $id)
    {
        $rules = [
            'leave_type_name' => 'required',
            'day_amount' => 'required',
            'year' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("/masterdata-payrollmaster-leavetypemaster")->withErrors($validator)->withInput();
        }


        $leave_type = leave_type::find($id);

        // ini untuk narik yang udah diinput
        $leave_type->leave_type_name = $request['leave_type_name'];
        $leave_type->day_amount = $request['day_amount'];
        $leave_type->year = $request['year'];
        $leave_type->save();
        // untuk meredirect dan memberikan notif sukses

        // Alert::success($leave_type->leave_type_name . ' has been Edited', 'Success Edited !')->persistent("Close");
        return redirect("/masterdata-payrollmaster-leavetypemaster")->with('success',$leave_type->leave_type_name . ' has been edited.');
    }
    // --------------------------Leave Type section-----------------------------------------//


    // -----------------------------Incentive----------------------------------------------------

    public function IncentiveMasterIndex()
    {
        $incentive = incentive::orderby('id', 'desc')->get();

        return view('master_data.PayrollMaster.IncentiveMaster', compact('incentive'));
    }
    public function IncentiveMasterStore(Request $request)
    {
        // dd('asdad');

        $rules = [
            'incentive_name' => 'required',
            'salary' => 'required',
            'year' => 'required'

        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("masterdata-payrollmaster-incentivemaster")->withErrors($validator)->withInput();
        }

        $newIncentive = new incentive();
        $newIncentive->incentive_name = $request['incentive_name'];
        $newIncentive->salary_amount = $request['salary'];
        $newIncentive->year = $request['year'];
        $newIncentive->save();

        // Alert::success('New Incentive Master has been added', 'Success Added !')->persistent("Close");

        return redirect("masterdata-payrollmaster-incentivemaster")->with('success','New Incentive has beed added.');
    }

    public function deleteIncentive($id)
    {
        $incentive = incentive::find($id);
        $incentive->delete();

        // Alert::success($incentive->incentive_name . ' has been deleted', 'Success Deleted !')->persistent("Close");

        return redirect("/masterdata-payrollmaster-incentivemaster")->with('success',$incentive->incentive_name .' has been deleted.');
    }

    public function editIncentive($id)
    {

        $incentive = incentive::find($id);

        return view('master_data.PayrollMaster.IncentiveMaster', ['incentive' => $incentive]);
    }

    public function doeditIncentive(Request $request, $id)
    {
        $rules = [
            'incentive_name' => 'required',
            'salary' => 'required',
            'year' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("/masterdata-payrollmaster-incentivemaster")->withErrors($validator)->withInput();
        }
        $incentive = incentive::find($id);
        // ini untuk narik yang udah diinput
        $incentive->incentive_name = $request['incentive_name'];
        $incentive->salary_amount = $request['salary'];
        $incentive->year = $request['year'];
        $incentive->save();
        // untuk meredirect dan memberikan notif sukses
        // Alert::success($incentive->incentive_name . ' has been Edited', 'Success Edited !')->persistent("Close");
        return redirect("/masterdata-payrollmaster-incentivemaster")->with('success',$incentive->incentive_name .' has been edited.');
    }
    // -------------------------------End Incentive------------------------------------------


    // ------------------------------------PPH Master------------------------------------------
    public function PPHTypeMasterIndex()
    {
        $pph_type = PPH::orderby('id', 'desc')->get();

        return view('master_data.PayrollMaster.PPHTypeMaster', compact('pph_type'));
    }

    public function PPHTypeMasterStore(Request $request)
    {
        // dd('asdad');

        $rules = [
            'pph_percentage' => 'required',
            'year' => 'required'


        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("masterdata-payrollmaster-PPHtypemaster")->withErrors($validator)->withInput();
        }

        $newPPH = new PPH();
        $newPPH->pph_percentage = $request['pph_percentage'];
        $newPPH->year = $request['year'];
        $newPPH->save();

        // Alert::success('New PPH Master has been added', 'Success Added !')->persistent("Close");

        return redirect("masterdata-payrollmaster-PPHtypemaster")->with('success', 'New PPH Type has been added.');
    }
    public function deletePPH($id)
    {
        $pph_type = PPH::find($id);
        $pph_type->delete();

        // Alert::success('PPH has been deleted', 'Success Deleted !')->persistent("Close");

        return redirect("/masterdata-payrollmaster-PPHtypemaster")->with('success','PPH Type has been deleted.');
    }
    public function editPPH($id)
    {

        $pph_type = PPH::find($id);

        return view('master_data.PayrollMaster.PPHTypeMaster', compact('pph_type'));
    }

    public function doeditPPH(Request $request, $id)
    {
        $rules = [
            'pph_percentage' => 'required',
            'year' => 'required'

        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("/masterdata-payrollmaster-PPHtypemaster")->withErrors($validator)->withInput();
        }
        $pph_type = PPH::find($id);
        $pph_type->pph_percentage = $request['pph_percentage'];
        $pph_type->year = $request['year'];
        $pph_type->save();
        // untuk meredirect dan memberikan notif sukses
        // Alert::success(' PPH  has been Edited', 'Success Edited !')->persistent("Close");
        return redirect("/masterdata-payrollmaster-PPHtypemaster")->with('success','PPH Type has been edited');
    }

    // ---------------------------------End PPH-------------------------------------------


    // --------------------------------------PTKP---------------------------------------
    public function PTKPTypeMasterIndex()
    {
        $ptkp = PTKP::orderby('id', 'desc')->get();

        return view('master_data.PayrollMaster.PTKPTypeMaster', compact('ptkp'));
    }
    public function  PTKPTypeMasterStore(Request $request)
    {
        $rules = [
            'jenis_ptkp' => 'required',
            'nominal_ptkp' => 'required',
            'jumlah_anak' => 'required',
            'year' => 'required'

        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("/masterdata-payrollmaster-PTKPtypemaster")->withErrors($validator)->withInput();
        }

        $newPTKP = new PTKP();
        $newPTKP->jenis_ptkp = $request['jenis_ptkp'];
        $newPTKP->nominal_ptkp = $request['nominal_ptkp'];
        $newPTKP->jumlah_anak = $request['jumlah_anak'];
        $newPTKP->year = $request['year'];

        $newPTKP->save();


        // Alert::success('PTKP Type has been added', 'Success Added !')->persistent("Close");
        return redirect("/masterdata-payrollmaster-PTKPtypemaster")->with('success','New PTKP has been added.');
    }

    public function deletePTKP($id)
    {
        $ptkp = PTKP::find($id);
        $ptkp->delete();

        // Alert::success('PTKP has been deleted', 'Success Deleted !')->persistent("Close");

        return redirect("/masterdata-payrollmaster-PTKPtypemaster")->with('success',$ptkp->jenis_ptkp. '  has been deleted.');
    
    }

    public function editPTKP($id)
    {

        $ptkp = PTKP::find($id);

        return view('masterdata-payrollmaster-PTKPtypemaster', compact('ptkp'));
    }

    public function doeditPTKP(Request $request, $id)
    {
        $rules = [
            'jenis_ptkp' => 'required',
            'nominal_ptkp' => 'required',
            'jumlah_anak' => 'required',
            'year' => 'required'

        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("/masterdata-payrollmaster-PTKPtypemaster")->withErrors($validator)->withInput();
        }
        $ptkp = PTKP::find($id);
        $ptkp->jenis_ptkp = $request['jenis_ptkp'];
        $ptkp->nominal_ptkp = $request['nominal_ptkp'];
        $ptkp->jumlah_anak = $request['jumlah_anak'];
        $ptkp->year = $request['year'];
        $ptkp->save();

        // untuk meredirect dan memberikan notif sukses
        Alert::success(' PTKP  has been Edited', 'Success Edited !')->persistent("Close");
        
        return redirect("/masterdata-payrollmaster-PTKPtypemaster")->with('success',$ptkp->jenis_ptkp.' has been edited.');
    
    }
    // --------------------------- End PTKP ----------------------------------------------

    public function WorkingDaysMasterIndex()
    {

        return view('master_data.PayrollMaster.WorkingDaysMaster');
    }
}
