<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\leaveSub;
use App\User;
use App\LeaveType;
use Illuminate\Support\Facades\Auth;

class ReportingController extends Controller
{
    public function DayOffRepindex(){
        $user = Auth::user();
        // $leave_history = leaveSub::all();
        $leave_history = leaveSub::orderby('id', 'desc')->where('user_auth', Auth::user()->id)
        ->selectRaw('submission_form_leave.id as id ,submission_form_leave.day_amount_sub,
        submission_form_leave.reason,submission_form_leave.start_date,
        submission_form_leave.end_date,submission_form_leave.status,
        u.name as user_id  , lt.leave_type_name as leave_type_id ')
        ->leftJoin('users as u', 'u.id', '=', 'submission_form_leave.user_id')
        ->leftJoin('leave_type as lt', 'lt.id', '=', 'submission_form_leave.leave_type_id')
        ->get();
        return view('reporting.DayOffRep', ['leavehistory' => $leave_history]);
    }

    // public function dataEmployee1()
    // {
    //     $dataemployee1 = User::orderby('id', 'desc')
    //         ->selectRaw('users.id as id ,users.nip,users.gender,users.private_email,users.region,users.education,users.placeofbirth,
    //                         users.phone_number,users.emergency_number,users.emergency_name,users.address, users.created_at, users.name as name,
    //                         users.status,users.status_karyawan,users.email, p.name as position , d.name as department, dv.name as division')
    //         ->leftJoin('position as p', 'p.id', '=', 'users.position_id')
    //         ->leftJoin('department as d', 'd.id', '=', 'users.department_id')
    //         ->leftJoin('division as dv', 'dv.id', '=', 'users.division_id')
    //         ->get();
    //     return view('master_data.employeemaster_byhrd.index', ['users' => $dataemployee1]);
    // }

    public function LoanRepindex(){

        return view('reporting.LoanRep');
    
    }

    public function OvertimeRepindex(){

        return view('reporting.OvertimeRep');

    }

    public function PaySlipRepindex (){

        return view('reporting.PaySlipRep');
    }

    public function PaySlipDetail(){
        
        return view('reporting.PaySlipDetail.PaySlipDetail');
    }

    public function PaySlipPrint(){
        
        return view('reporting.PaySlipDetail.PaySlipPrint');
    }

}
