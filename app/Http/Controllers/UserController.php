<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\User;
use Excel;
use Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;
//Enables us to output flash messaging
use Session;
use App\department;
use Illuminate\Support\Facades\Response;
use App\Division;
use App\Position;
use App\PTKP;
use Alert;
use App\Bank;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'clearance'])->except('show');
    }

    // search user view
    public function searchuser(Request $request)
    {
        $users = User::selectRaw('users.id, users.name,users.email,users.position,users.created_at')
            ->whereRaw(
                'users.name = ? or users.email = ? or users.position = ?',
                [$request->search, $request->search, $request->search]
            )
            ->paginate(10);
        $search = $request->search;

        return view('users.index', compact('users', 'search'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // -----------------------------------------------------------------------------------------------------------------------------------
    //  INI FUNCTION UNTUK EMPLOYEE MASTER BY ADMIN

    // Ini view data employee
    public function index()
    {
        $datauser = User::orderby('created_at', 'desc')
            ->selectRaw('users.id as id ,users.nip,users.gender,users.private_email,users.region,users.education,users.placeofbirth,
            users.phone_number,users.emergency_number,users.emergency_name,users.address, users.created_at, users.name as name,
            users.status,users.status_karyawan,users.email, p.name as position , d.name as department, dv.name as division')
            ->leftJoin('position as p', 'p.id', '=', 'users.position_id')
            ->leftJoin('department as d', 'd.id', '=', 'users.department_id')
            ->leftJoin('division as dv', 'dv.id', '=', 'users.division_id')
            ->get();

        return view('master_data.employeemaster_byadmin.index', ['users' => $datauser]);
    }
    // add employee data
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Get all roles and pass it to the view
        $roles = Role::get();
        return view('master_data.employeemaster_byadmin.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name, email and password fields
        $rules = [
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users',
        ];
        $validator = Validator::make($request->all(), $rules);
        // if ($validator->fails()) {
        //     return redirect('users/create')->withErrors($validator)->withInput();
        // }
        if ($validator->fails()) {
            // $notification = array(
            //     'message' => '',
            //     'alert-type' => 'error'
            // );
            // return redirect("users/$id/edit")->withErrors($validator)->withInput();
            return redirect('users/create')->withErrors($validator)->withInput();
        }
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $password = "Emportal123";
        $user->password = $password;
        $roles = $request['roles']; //Retrieving the roles field
        $user->save();
        //Checking if a role was selected
        if (isset($roles)) {
            foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();
                $user->assignRole($role_r); //Assigning role to user
            }
        }
        //Redirect to the users.index view and display message
        // $notification = array(
        //     'message' => 'User ' . $user->name . ' Added successfully !',
        //     'alert-type' => 'success'
        // );
        // Alert::success('User has been added', 'Success')->persistent("Close");
        return redirect()->route('users.index')->with('success','New User has been added. ');
    }
    // end add employee data
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //  get user to edit view
    public function edit($id)
    {
        $roles = Role::get(); //Get all roles
        $users = User::find($id);
        return view('master_data.employeemaster_byadmin.edit', compact('users', 'roles')); //pass user and roles data to view
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //  do update user
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        //Validate name, email and password fields    
        $rules = [
            'name' => 'required|max:120',
            'email' => 'required',
            'password' => 'confirmed',

        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("users/$id/edit")->withErrors($validator)->withInput();
        }

        $user = User::find($id);
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->password = $request['password'];

        $roles = $request['roles']; //Retreive all roles
        if (isset($roles)) {
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles          
        } else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        $user->save();
        // Alert::success('User ' . $user->name . ' has been updated ', 'Success')->persistent("Close");
        return redirect()->route('users.index')->with('success',$user->name . ' Has been edited. ');
    }
    // delete user by admin
    public function deleteUser($id)
    {
        $user = User::find($id); /*Mengambil data user sesuai id user*/
        $user->delete(); /*Menghapus user yang dipilih sesuai id user*/
        Alert::success('User ' . $user->name . ' has been deleted', 'Success Deleted !')->persistent("Close");
        // $notification = array(
        //     'message' => 'User ' . $user->name . ' Deleted successfully !',
        //     'alert-type' => 'success'
        // );
        return redirect()->route('users.index')->with('success',$user->name. ' Has been deleted. ');
    }
    // export to excel
    public function userExportAdmin()
    {
        $users = User::all();
        $array_role = [];
        foreach ($users as $user) {
            $role = $user->roles()->pluck('name')->implode(' ');
            array_push($array_role, $role);
        }
        $user = User::select('name', 'email')->get();
        return Excel::create('data_userByAdmin', function ($excel) use ($user, $array_role) {
            $excel->sheet('mysheet', function ($sheet) use ($user, $array_role) {
                $sheet->fromArray($user);
                $sheet->data = [];
                $sheet->setCellValue('C1', 'Role');
                $row = 2;
                foreach ($array_role as $role) {
                    $sheet->setCellValue(('C' . $row), $role);
                    $row++;
                }
            });
        })->download('xls');
    }
    // add user using import excel

    public function userImportAdmin(Request $request)
    {
        if ($request->hasFile('file')) {
            $path = $request->file('file')->getRealPath();
            $data = Excel::load($path, function ($reader) { })->get();
            if (!empty($data) && $data->count()) {
                $flag = 0;
                $nip_error = "";
                $email_error = "";
                foreach ($data as $key => $value) {
                    $check_user_nip = User::whereRaw('nip = ?', [$value->nip])->get();
                    $check_user_email = User::whereRaw('email = ?', [$value->email])->get();

                    if (count($check_user_nip) > 0) {
                        $flag = 1;
                        $nip_error = $nip_error . $value->nip . " has been registered." . "-";
                    } elseif (count($check_user_email) > 0) {
                        $flag = 1;
                        $email_error = $email_error . $value->email . " has been registered." . "-";
                    } else {
                        $user = new User();
                        $user->name = $value->name;
                        $user->email = $value->email;
                        $password = "Emportal123";
                        $user->password = $password;
                        // $roles = $request['roles']; //Retreive all roles
                        // if (isset($roles)) {
                        //     $user->roles()->sync($roles);  //If one or more role is selected associate user to roles          
                        // } else {
                        //     $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
                        // }
                        $user->save();
                    }
                }
                if ($flag == 1) {
                    $error = substr($nip_error, 0, -1) . "*" . substr($email_error, 0, -1);
                    return back()->with('error', $error);
                }
            }
        }
        Alert::success('User has been added', 'Success')->persistent("Close");
        return back();
    }
    // donwload template excel by ADMIN
    public function downloadtemplateAdmin()
    {
        $file = public_path() . "/files/import-template-byAdmin.xls";
        return Response::download($file, 'import-template-byAdmin.xls');
    }
    // END EMPLOYEE_MASTER BY ADMIN
    // ------------------------------------------------------------------------------------------------------------------------------------------

    //  GOOD JOB !!!!
    // --------------------------------------------------------------------------------------------------------------------------------------------
    // INI FUNCTION UNTUK EMPLOYEE MASTER BY HRD
    //  view data employee
    public function dataEmployee1()
    {
        $dataemployee1 = User::orderby('id', 'desc')
            ->selectRaw('users.id as id ,users.nip,users.gender,users.private_email,users.region,users.education,users.placeofbirth,
                            users.phone_number,users.emergency_number,users.emergency_name,users.address, users.created_at, users.name as name,
                            users.status,users.status_karyawan,users.email, p.name as position , d.name as department, dv.name as division')
            ->leftJoin('position as p', 'p.id', '=', 'users.position_id')
            ->leftJoin('department as d', 'd.id', '=', 'users.department_id')
            ->leftJoin('division as dv', 'dv.id', '=', 'users.division_id')
            ->get();
        return view('master_data.employeemaster_byhrd.index', ['users' => $dataemployee1]);
    }
    
    // edit employee
    public function editEmployee($id)
    {


        $department = department::all();
        $division = Division::all();
        $position = Position::all();
        $bank = Bank::all();
        $users = User::find($id);
        return view('master_data.employeemaster_byhrd.edit', compact('users', 'department', 'division', 'position', 'bank','supervisors')); //pass user
    }

    public function doEditEmployee(Request $request, $id)
    {
        /*Validator update user Account*/
        $rules = [
            'name' => 'required|max:120',
            'email' => 'required',
            'nip' => 'required',
            'no_rekening' => 'required',
            'default_leave'=>'required',
            'bank' => 'required',
            'region' => 'required|max:120',
            'gender' => 'required',
            'education' => 'required',
            'placeofbirth' => 'required',
            'birthofdate' => 'required',
            'address' => 'required',
            'status' => 'required',
            'department' => 'required',
            'division' => 'required',
            'position' => 'required',
            'status_karyawan' => 'required',
            'private_email' => 'required',
            'phone_number' => 'required',
            'emergency_number' => 'required',
            'emergency_name' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("edit-employee/$id")->withErrors($validator)->withInput();
        }
        $user = User::find($id);
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->nip = $request['nip'];
        $user->bank_id = $request['bank'];
        $user->no_rekening = $request['no_rekening'];
        $user->default_leave = $request['default_leave'];
        $user->region = $request['region'];
        $user->gender = $request['gender'];
        $user->education = $request['education'];
        $user->placeofbirth = $request['placeofbirth'];
        $user->birthofdate = $request['birthofdate'];
        $user->address = $request['address'];
        $user->status = $request['status'];
        $user->division_id = $request['division'];
        $user->position_id = $request['position'];
        $user->status_karyawan = $request['status_karyawan'];
        $user->private_email = $request['private_email'];
        $user->department_id = $request['department'];
        $user->phone_number = $request['phone_number'];
        $user->emergency_number = $request['emergency_number'];
        $user->emergency_name = $request['emergency_name'];
        $user->save();
        Alert::success('User ' .  $user->name    . ' has been updated', 'Success')->persistent("Close");
        return redirect("master-employee-detail");
    }
    // donwload template excel by ADMIN
    public function downloadtemplateHrd()
    {
        $file = public_path() . "/files/import-template-byHRD.xls";
        return Response::download($file, 'import-template-byHRD.xls');
    }


    // END FUNCTION EMPLOYEE MASTER BY HRD
    // ============================================================================================================================================


    // --------------------------------------------------------------------------------------------------------------------
    //  INI UNTUK FUNCTION DI MAIN MENU
    // employee Data
    public function dataEmployee()
    {
        $dataemployee = User::orderby('id', 'desc')
            ->selectRaw('users.id as id ,users.nip,users.gender,users.private_email,users.region,users.education,users.placeofbirth,
                            users.phone_number,users.emergency_number,users.emergency_name,users.address, users.created_at, users.name as name,
                            users.status,users.status_karyawan,users.email, p.name as position , d.name as department, dv.name as division')
            ->leftJoin('position as p', 'p.id', '=', 'users.position_id')
            ->leftJoin('department as d', 'd.id', '=', 'users.department_id')
            ->leftJoin('division as dv', 'dv.id', '=', 'users.division_id')
            ->get();

        return view('employee_data.data-employee', ['users' => $dataemployee]);
    }
    // detail employee
    public function detail($id)
    {
        $department = department::all();
        $division = Division::all();
        $position = Position::all();
        $bank = Bank::all();
        $users = User::orderby('id', 'desc')
            ->selectRaw('users.id as id ,users.nip,users.gender,users.private_email,users.region,users.education,users.placeofbirth,users.birthofdate,users.no_rekening,
                            users.phone_number,users.emergency_number,users.emergency_name,users.address, users.created_at, users.name as name,
                            users.status,users.status_karyawan,users.email, p.name as position , d.name as department, dv.name as division,b.name as bank, profile_picture')
            ->leftJoin('position as p', 'p.id', '=', 'users.position_id')
            ->leftJoin('department as d', 'd.id', '=', 'users.department_id')
            ->leftJoin('division as dv', 'dv.id', '=', 'users.division_id')
            ->leftJoin('bank as b', 'b.id', '=', 'users.bank_id')
            ->where('users.id', $id)
            ->first();

        return view('employee_data.detail', compact('users', 'department', 'division', 'position', 'bank')); //pass user and roles data to view
    }

    public function profile()
    {
        //Get all roles and pass it to the view

        $id = Auth::user()->id;

        $datauser = User::orderby('id', 'desc')
            ->selectRaw('users.id as id ,users.nip,users.gender,users.private_email,users.region,users.education,users.placeofbirth,users.birthofdate,users.no_rekening,
                            users.phone_number,users.emergency_number,users.emergency_name,users.address, users.created_at, users.name as name,users.default_leave,
                            users.status,users.status_karyawan,users.email, p.name as position , d.name as department, dv.name as division, b.name as bank,profile_picture')
            ->leftJoin('position as p', 'p.id', '=', 'users.position_id')
            ->leftJoin('bank as b', 'b.id', '=', 'users.bank_id')
            ->leftJoin('department as d', 'd.id', '=', 'users.department_id')
            ->leftJoin('division as dv', 'dv.id', '=', 'users.division_id')
            ->where('users.id', $id)
            ->first();

        return view('users.profile', ['user' => $datauser]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('users');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find a user with a given id and delete
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('users.index')
            ->with(
                'flash_message',
                'User successfully deleted.'
            );
    }
    // export data employee 
    public function userExport()
    {
        $user = User::orderby('nip','desc')
            ->selectRaw('users.nip, users.name as name, p.name as position , d.name as department, dv.name as division ,b.name as bank,users.no_rekening ,users.gender,users.email,users.private_email,users.region,users.education,users.placeofbirth,users.birthofdate,
                            users.phone_number,users.emergency_number,users.emergency_name,users.address, 
                            users.status,users.status_karyawan')
            ->leftJoin('position as p', 'p.id', '=', 'users.position_id')
            ->leftJoin('department as d', 'd.id', '=', 'users.department_id')
            ->leftJoin('division as dv', 'dv.id', '=', 'users.division_id')
            ->leftJoin('bank as b', 'b.id', '=', 'users.bank_id')
            ->get();
        return Excel::create('data_user', function ($excel) use ($user) {
            $excel->sheet('mysheet', function ($sheet) use ($user) {
                $sheet->fromArray($user);
            });
        })->download('xls');
    }



    // update and delete picture profile
    public function updatepicture(Request $request)
    {

        $user = User::find($request->id);
        if ($request->file('profile_picture') != null) {
            $rand = rand();
            if ($user->profile_picture != null) {
                unlink('images/profile/' . $user->profile_picture);
            }

            $file = $request->file('profile_picture');
            $file->move(public_path('/images/profile'), $rand . $file->getClientOriginalName());
            $user->profile_picture = $rand . $file->getClientOriginalName();
            $user->save();
        }

        return back()->with('update_pp', 'Picture has been updated.');
    }
    public function removepicture($id)
    {
        $user = User::find($id);
        unlink('images/profile/' . $user->profile_picture);
        $user->profile_picture = null;
        $user->save();
        return back()->with('remove_pp', 'Picture has been removed.');
    }
    public function userImport(Request $request)
    {
        if ($request->hasFile('file')) {
            $path = $request->file('file')->getRealPath();
            $data = Excel::load($path, function ($reader) { })->get();
            if (!empty($data) && $data->count()) {
                $flag = 0;
                $nip_error = "";
                $email_error = "";

                foreach ($data as $key => $value) {
                    $check_user_nip = User::whereRaw('nip = ?', [$value->nip])->get();
                    $check_user_email = User::whereRaw('email = ?', [$value->email])->get();

                    if (count($check_user_nip) > 0) {
                        $flag = 1;
                        $nip_error = $nip_error . $value->nip . " has been registered." . "-";
                    } elseif (count($check_user_email) > 0) {
                        $flag = 1;
                        $email_error = $email_error . $value->email . " has been registered." . "-";
                    } else {
                        $user = new User();
                        $user->nip = $value->nip;
                        $user->department_id = $value->department;
                        $user->position_id = $value->position;
                        $user->division_id = $value->division;
                        $user->name = $value->name;
                        $user->profile_picture = $value->profile_picture;
                        $user->education = $value->education;
                        $user->email = $value->email;
                        $user->gender = $value->gender;
                        $user->private_email = $value->private_email;
                        $user->region = $value->region;
                        $user->phone_number = $value->phone_number;
                        $user->birthofdate = $value->birthofdate;
                        $user->status_karyawan = $value->status_karyawan;
                        $user->address = $value->address;
                        $user->status = $value->status;

                        $dob = date('dmY', strtotime($value->birthofdate));
                        $password = "Emportal" . $dob;
                        $user->password = $password;

                        $user->save();
                    }
                }

                if ($flag == 1) {
                    $error = substr($nip_error, 0, -1) . "*" . substr($email_error, 0, -1);

                    return back()->with('error', $error);
                }
            }
        }
        return back();
    }

    public function downloadtemplate()
    {
        //  return Excel::load(public_path().'/files/import-template.xls', function($excel)  {
        // //     // $excel->sheet('mysheet', function($sheet)
        // //     // {
        // //     //     $sheet->setCellValue('A1', 'nip');
        // //     //     $sheet->setCellValue('B1', 'department');
        // //     //     $sheet->setCellValue('C1', 'name');
        // //     //     $sheet->setCellValue('D1', 'education');
        // //     //     $sheet->setCellValue('E1', 'email');
        // //     //     $sheet->setCellValue('F1', 'position');
        // //     //     $sheet->setCellValue('G1', 'birthofdate');
        // //     //     $sheet->setCellValue('H1', 'address');
        // //     //     $sheet->setCellValue('I1', 'status');

        // //     // });
        // })->download('xls');

        $file = public_path() . "/files/import-template.xls";
        return Response::download($file, 'import-template.xls');
    }


    // Ini function untuk dibagian SETTINGS
    // update account
    public function updateAccount($id)
    {
        /*Mengambil data user sesuai id untuk dibawa ke view update-account*/

        $updateAccount = User::find($id);
        return view('users.update-account', ['users' => $updateAccount]);
    }

    public function doUpdateAccount(Request $request, $id)
    {

        /*Validator update user Account*/
        $rules = [
            'password' => 'required',
            'password' => 'confirmed',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("update-account/$id")->withErrors($validator)->withInput();
        }

        /*Mengambil data user sesuai id untuk di update dengan data yang baru*/
        $user = User::find($id);
        // ini untuk validasi
        // $user->name = $request['name'];
        $user->password = $request['password'];
        $user->save();
        // untuk meredirect dan memberikan notif sukses
        return redirect("update-account/$id")->with('success', 'Data account has been updated.');
    }

    // update profile
    public function updateProfile($id)
    {
        /*Mengambil data user sesuai id untuk dibawa ke view update-account*/

        $updateProfile = User::find($id);
        return view('users.update-profile', ['users' => $updateProfile]);
    }
    public function doUpdateProfile(Request $request, $id)
    {

        /*Validator update user Account*/
        $rules = [
            'name' => 'required',
            'education' => 'required',
            'address' => 'required',
            'status' => 'required',
            'private_email' => 'required',
            'phone_number' => 'required',
            'emergency_number' => 'required',
            'emergency_name' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect("update-profile/$id")->withErrors($validator)->withInput();
        }

        /*Mengambil data user sesuai id untuk di update dengan data yang baru*/
        $user = User::find($id);
        // ini untuk validasi
        $user->name = $request['name'];
        $user->education = $request['education'];
        $user->address = $request['address'];
        $user->status = $request['status'];
        $user->private_email = $request['private_email'];
        $user->phone_number = $request['phone_number'];
        $user->emergency_number = $request['emergency_number'];
        $user->emergency_name = $request['emergency_name'];
        $user->save();
        // untuk meredirect dan memberikan notif sukses
        return redirect("update-profile/$id")->with('success', 'Data profile has been updated.');
    }


    // END FUNCTION MAIN MENU
    // ------------------------------------------------------------------------------------------------------------------------



}
