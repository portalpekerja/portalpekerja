@extends('layouts.app-spinner')
@section('content')
<section class="content">

    <body class="fix-header fix-sidebar card-no-border">
        <div id="main-wrapper">
            <div class="content-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->
                    <div class="row page-titles">
                        <div class="col-md-12 col-8 align-self-center">
                            <h3 class="text-themecolor">Attendance</h3>
                        </div>
                        <div class="col-md-12 col-8 align-self-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Attendance</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-info"><i
                                    class="mdi mdi-calendar-check"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-light">5</h3>
                                <h5 class="text-muted m-b-0">Remaining Days Off</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-success"><i
                                    class="mdi mdi-clock"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-light">5</h3>
                                <h5 class="text-muted m-b-0">Overtime Total</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-primary"><i class="ti-bar-chart"></i>
                            </div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-lgiht"> Excellent</h3>
                                <h5 class="text-muted m-b-0">Last Month's Performance Result</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Summary Attendance</h4>
                    <div class="table-responsive m-t-40">
                        <table id="example23" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Date</th>
                                    <th>Check In</th>
                                    <th>Check Out</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <?php $no = 0?>
                            <tbody>
                                <tr>
                                    <td>2</td>
                                    <td>Date</td>
                                    <td>Jam Masuk</td>
                                    <td>Jam Keluar</td>
                                    <td>Telat</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Date</td>
                                    <td>Jam Masuk </td>
                                    <td>Jam Keluar</td>
                                    <td>Absen</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Jibon</td>
                                    <td>Jam Masuk</td>
                                    <td>Jam Keluar</td>
                                    <td>Telat</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </body>
    <script>
            $(function () {
            $('#myTable').DataTable();
            $(function () {
            var table = $('#example').DataTable({
            "columnDefs": [{
            "visible": false,
            "targets": 2
            }],
            "order": [
            [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function (settings) {
            var api = this.api();
            var rows = api.rows({
            page: 'current'
            }).nodes();
            var last = null;
            api.column(2, {
            page: 'current'
            }).data().each(function (group, i) {
            if (last !== group) {
            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
            last = group;
            }
            });
            }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
            table.order([2, 'desc']).draw();
            } else {
            table.order([2, 'asc']).draw();
            }
            });
            });
            });
            $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'csv', 'excel', 'pdf'
            ]
            });
    </script>
</section>
@endsection