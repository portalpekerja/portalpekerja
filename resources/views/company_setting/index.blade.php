@extends('layouts.app-spinner')
@section('content')

<section class="content">
    @include('layouts.partials.alerts')

    <body class="fix-header fix-sidebar card-no-border">
        <div id="main-wrapper">
            <div class="content-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->
                    <div class="row page-titles">
                        <div class="col-md-12 col-8 align-self-center">
                            <h3 class="text-themecolor"> Company Setting</h3>
                        </div>
                        <div class="col-md-12 col-8 align-self-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item">Setting</li>
                                <li class="breadcrumb-item active">Company</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><i class=" ti-settings"></i> Company Settings</h4>
                        <hr>
                        {{ Form::open(array('url' => 'users')) }}
                        @if ($errors->any())
                        <div class="form-group">
                            <div class="alert alert-danger col-md-12">
                                {{ $errors->first() }}
                            </div>
                        </div>
                        @endif
                        <div class="form-group">
                            <div class="card-body">
                                <label for="pwd1">Company Name</label>
                                <input type="text" class="form-control" name="name_company">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="card-body">
                                <label for="input-file-now-custom-1">Upload Company Logo</label>
                                <input type="file" id="input-file-now" class="dropify" />
                            </div>
                        </div>
                        <div style="text-align: center">
                            {{ Form::submit('Submit', array('class' => 'btn btn-success')) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                        // Basic
                        $('.dropify').dropify();
                        // Translated
                        $('.dropify-fr').dropify({
                            messages: {
                                default: 'Glissez-déposez un fichier ici ou cliquez',
                                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                                remove: 'Supprimer',
                                error: 'Désolé, le fichier trop volumineux'
                            }
                        });
                        // Used events
                        var drEvent = $('#input-file-events').dropify();
                        drEvent.on('dropify.beforeClear', function(event, element) {
                            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                        });
                        drEvent.on('dropify.afterClear', function(event, element) {
                            alert('File deleted');
                        });
                        drEvent.on('dropify.errors', function(event, element) {
                            console.log('Has Errors');
                        });
                        var drDestroy = $('#input-file-to-destroy').dropify();
                        drDestroy = drDestroy.data('dropify')
                        $('#toggleDropify').on('click', function(e) {
                            e.preventDefault();
                            if (drDestroy.isDropified()) {
                                drDestroy.destroy();
                            } else {
                                drDestroy.init();
                            }
                        })
                    });
        </script>
    </body>
</section>

@endsection