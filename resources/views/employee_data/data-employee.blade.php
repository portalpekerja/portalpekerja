@extends('layouts.app-spinner')
@section('content') 

<section class="content">
        <body class="fix-header fix-sidebar card-no-border">
            <div id="main-wrapper">
                <div class="content-wrapper">
                    <!-- ============================================================== -->
                    <!-- Container fluid  -->
                    <!-- ============================================================== -->
                    <div class="container-fluid">
                        <!-- ============================================================== -->
                        <!-- Bread crumb and right sidebar toggle -->
                        <!-- ============================================================== -->
                        <div class="row page-titles">
                            <div class="col-md-12 col-8 align-self-center">
                                <h3 class="text-themecolor">Employee Data</h3>
                            </div>
                            <div class="col-md-12 col-8 align-self-center">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                    <li class="breadcrumb-item active">Employee Data</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                    <div class="card-body">
                        <div >
                            <a style="color: white" href="{{url('export-user')}}" class="float-right btn-sm btn-default btn-success"> Export data to Excel (.xls)
                            </a> 
                        </div>
                        <h4 class="card-title">Employee Data</h4>
                        <h6 class="card-subtitle">View Data Employee</h6>
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Employee ID</th>
                                        <th>Name</th>
                                        <th>Department</th>
                                        <th>Position</th>
                                        <th>Employee Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0?>
                                    @foreach ($users as $user)
                                    <?php $no++?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{ $user->nip }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->department }}</td>
                                        <td>{{ $user->position}}</td>
                                        <td>{{ $user->status_karyawan }}</td>
                                        <td>
                                            <div class="tools" style="padding-left: 20px">
                                                <a href="{{route('users.detail',$user->id) }}" 
                                                    style="margin-right: 3px;">Detail</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        </body>
        <script>
                $(function () {
                    $('#myTable').DataTable();
                    $(function () {
                    var table = $('#example').DataTable({
                    "columnDefs": [{
                    "visible": false,
                    "targets": 2
                    }],
                    "order": [
                    [2, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                    page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                    page: 'current'
                    }).data().each(function (group, i) {
                    if (last !== group) {
                    $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                    last = group;
                    }
                    });
                    }
                    });
                    // Order by the grouping
                    $('#example tbody').on('click', 'tr.group', function () {
                    var currentOrder = table.order()[0];
                    if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                    } else {
                    table.order([2, 'asc']).draw();
                    }
                    });
                    });
                    });
                    $('#example23').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'csv', 'excel', 'pdf'
                    ]
                    });
                    function eximForm(){
                    $('#modal-exim').modal('show');
                    $('#modal-exim form')[0].reset();
                } 
            </script>
</section>


@endsection