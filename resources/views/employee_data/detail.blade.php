@extends('layouts.app-spinner')
@section('content')

<section class="content">
    <style>
        .image {
            opacity: 1;
            display: block;
            width: 150px;
            height: 150px;
            transition: .5s ease;
            backface-visibility: hidden;
        }
        .middle {
            transition: .5s ease;
            opacity: 0;
            position: absolute;
            top: 25%;
            left: 50%;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            text-align: center;
        }

        .card-body:hover .image {
            opacity: 0.3;
        }

        .card-body:hover .middle {
            opacity: 1;
        }

        .text {
            color: white;
            font-size: 15px;
        }

    </style>

    <body class="fix-header fix-sidebar card-no-border">
        <div id="main-wrapper">
            <div class="content-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->
                    <div class="row page-titles">
                        <div class="col-md-12 col-8 align-self-center">
                            <h3 class="text-themecolor">Employee Detail</h3>
                        </div>
                        <div class="col-md-12 col-8 align-self-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Employee Detail</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <div class="card-body">
                        <center class="m-t-30">
                            <div class="ribbon ribbon-bookmark  ribbon-info">
                                Emp<i class="fa fa-user-circle"></i>rtal
                            </div>
                            @if($users->profile_picture == null)
                            <img style="width: 50%" src="{{asset('images/user.png')}}" class="img-circle image" />
                            @else
                            <img style="width: 50%" src="{{asset('images/profile/'.$users->profile_picture)}}"
                                class="img-circle image">
                            @endif
                            {{-- <form action="{{url('user-update-picture')}}" method="POST" enctype="multipart/form-data"
                                style="display: none">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="{{Auth::user()->id}}">
                                <input type="file" name="profile_picture" id="upload-profile" accept="image/*">
                                <button type="submit" id="submit-profile-picture"></button>
                            </form> --}}
                            {{-- <div class="middle">
                                <button id="trigger" class="btn btn-primary text"><i class="fa fa-edit"></i></button>
                                @if(Auth::user()->profile_picture != null)
                                <a href="{{url('user-remove-picture/'.Auth::user()->id)}}" id="remove-profile-picture"
                                    class="btn btn-danger text"><i class="fa fa-trash"></i></a>
                                @endif
                            </div> --}}
                            <h4 class="card-title m-t-10">{{$users->name}}</h4>
                            <h6 class="card-subtitle">{{$users->position}}</h6>
                        </center>
                    </div>
                    <div>
                    </div>
                    <div class="card-body">
                        <small class="text-muted">Employee ID</small>
                        <h6>{{$users->nip}}</h6>
                        <small class="text-muted">Email address</small>
                        <h6>{{$users->email}}</h6>
                        <small class="text-muted">Phone</small>
                        <h6>{{$users->phone_number}}</h6>
                        <small class="text-muted ">Address</small>
                        <h6>{{$users->address}}</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-3 col-md-5 ">
                <div class="card">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="home" role="tabpanel">
                            <div class="card-body">
                                <form class="form-horizontal" role="form">
                                    <div class="form-body">
                                        <h3 class="box-title">Personal Info</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Region :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->region}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Bank Name :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->bank}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Employee ID
                                                        :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->nip}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Region :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->region}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">No Rekening
                                                        :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->no_rekening}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Marital status
                                                        :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->status}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Gender :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->gender}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Last Education
                                                        :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->education}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Place of Birth
                                                        :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->placeofbirth}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Birth of Date
                                                        :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->birthofdate}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <!--/span-->
                                        </div>
                                        <h3 class="box-title">Company Info</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Department
                                                        :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->department}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4"> Division
                                                        :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->division}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Position :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->position}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4"> PTKP
                                                        :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->ptkp_id}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Employe Status:</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->status_karyawan}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <h3 class="box-title">Contact Info</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Emerge Name
                                                        :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->emergency_name}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4"> Emerg
                                                        Number:</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->emergency_number}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Comp Email
                                                        :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->email}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Private Email
                                                        :</label>
                                                    <div class="col-md-4">
                                                        <p class="form-control-static"> {{$users->private_email}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
    </body>
    <script>
        $(document).ready(function () {
                var div = $('.box-profile');          
                $('#trigger').click(function () {
                    var profile = $('#upload-profile');              
                    profile.click();
                    profile.change(function () {
                        $('#submit-profile-picture').click();
                        
                        div.addClass("disabledbox");
                    });
                });
                $('#submit-profile').click(function () {
                    div.addClass("disabledbox");
                });
            });
    </script>
    @endsection