@extends('layouts.app-spinner')
@section('content')


<section class="content">

    <body>
        <div id="main-wrapper">
            <div class="content-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->
                    <div class="row page-titles">
                        <div class="col-md-12 col-8 align-self-center">
                            <h3 class="text-themecolor">Employee Performance</h3>
                        </div>
                        <div class="col-md-12 col-8 align-self-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Employee Performance</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Value Performance</h4>
                        <h6 class="card-subtitle">Tahun : 2019</h6>
                        <hr>
                        <div class="ct-area-ln-chart" style="height: 400px;"></div>
                        <hr>
                        <div>
                            <h6 class="text-muted text-info"><i class="fa fa-circle font-10 m-r-10"></i> Performance
                                Information</h6>
                            <h6>Range [7 - 9] : <button class="btn btn-sm btn-rounded btn-success">EXCELLENT</button>
                            </h6>
                            <h6>Range [4 - 6] : <button class="btn btn-sm btn-rounded btn-info">GOOD</button></h6>
                            <h6>Range [0 - 3] : <button class="btn btn-sm btn-rounded btn-danger">BAD</button></h6>
                        </div>
                        <div class="float-right" style="bottom: 125px;position: relative">
                            <h6 class="text-muted text-info"><i class="fa fa-circle font-10 m-r-10"></i>Last Month
                                Performance Results</h6>
                            <h1 style="text-align: center"><button class="btn btn-sm btn-default btn-info">GOOD</button>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</section>
@endsection