<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Emportal</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    {{-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"> --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset ('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset ('bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset ('bower_components/Ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{ asset ('dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{ asset ('dist/css/AdminLTE.css')}}">
    <link rel="stylesheet" href="{{ asset ('dist/css/sweetalert2.css')}}">
    <link rel="stylesheet" href="{{ asset ('dist/css/sweetalert2.css')}}">
    <link rel="stylesheet" href="{{ asset ('dist/css/sweetalert2.min.css')}}">
    <link rel="stylesheet" href="{{ asset ('css/style.css')}}">
    <link rel="stylesheet" href="{{ asset ('dist/css/skins/skin-blue.min.css')}}">
    <link rel="stylesheet" href="{{ asset ('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset("dist/css/skins/_all-skins.min.css")}}">
    <link rel="stylesheet" type="text/css" href="/pathto/css/sweetalert.css">
    
    {{-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> --}}
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset("bower_components/jvectormap/jquery-jvectormap.css")}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{asset("bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css")}}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{asset("plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset("bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css")}}">
    <!-- Pace style -->
    <link rel="stylesheet" href="{{asset("plugins/pace/pace.min.css")}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset("bower_components/select2/dist/css/select2.min.css")}}">
    <!-- PNotify -->
    <link rel="stylesheet" href="{{asset("vendors/pnotify/pnotify.custom.min.css")}}">
    <link rel="stylesheet" href="sweetalert2.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    
    
    {{-- Script --}}
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="{{ asset( 'bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ asset ('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ('dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset ('dist/js/sweetalert2.all.js') }}"></script>
    <script src="{{ asset ('dist/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset ('dist/js/sweetalert2.js') }}"></script>
    <script src="{{ asset ('dist/js/sweetalert2.min.js') }}"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{{ asset ('plugins/input-mask/jquery.inputmask.js')}}"></script>
    <script src="{{ asset ('plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
    <script src="{{asset  ('plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
    <script src="{{asset("bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js")}}"></script>
    <script src="{{asset("bower_components/datatables.net/js/jquery.dataTables.min.js")}}"></script>
    <script src="{{asset("bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
    <script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    {{-- <script src="../../dist/js/demo.js"></script> --}}
    <script src="{{asset("bower_components/PACE/pace.min.js")}}"></script>
    <!-- Select2 -->
    <script src="{{asset("bower_components/select2/dist/js/select2.full.min.js")}}"></script>
    <script src="{{asset("vendors/Chart.js/dist/Chart.min.js")}}"></script>
    <script src="../../bower_components/chart.js/Chart.js"></script>
    <!-- PNotify -->
    <script src="{{asset("vendors/pnotify/pnotify.custom.min.js")}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ajaxStart(function() { Pace.restart(); });
    </script>
    <!-- Google Font -->
    
    
</head>
<script src="/pathto/js/sweetalert.js"></script>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        
        <!-- Main Header -->
        <header class="main-header">
            
            <!-- Logo -->
            <a href="{{url('dashboard')}}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>EP</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Emp<i class="fa fa-user-circle-o" aria-hidden="true"></i>rtal</span>
                    </a>
                    <!-- Header Navbar -->
                    <nav class="navbar navbar-static-top" role="navigation">
                        <!-- Sidebar toggle button-->
                        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">   
                        </a>
                        <!-- Navbar Right Menu -->
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                
                                <!-- Notifications Menu -->
                                <li class="dropdown notifications-menu">
                                    <!-- Menu toggle button -->
                                    {{-- <div class="servertime">
                                        <div id="clockDisplay"> </div>
                                    </div>  --}}
                                    <ul class="dropdown-menu">
                                        <li class="header">You have 10 notifications</li>
                                        <li>
                                            <!-- Inner Menu: contains the notifications -->
                                            <ul class="menu">
                                                <li>
                                                    <!-- start notification -->
                                                    <a href="#">
                                                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                                    </a>
                                                </li>
                                                <!-- end notification -->
                                            </ul>
                                        </li>
                                        <li class="footer"><a href="#">View all</a></li>
                                    </ul>
                                </li>
                                <!-- Tasks Menu -->
                                <li class="dropdown tasks-menu">
                                    <!-- Menu Toggle Button -->
                                    <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-flag-o"></i>
                                        <span class="label label-danger">9</span>
                                    </a> -->
                                    <ul class="dropdown-menu">
                                        <li class="header">You have 9 tasks</li>
                                        <li>
                                            <!-- Inner menu: contains the tasks -->
                                            <ul class="menu">
                                                <li>
                                                    <!-- Task item -->
                                                    <a href="#">
                                                        <!-- Task title and progress text -->
                                                        <h3>
                                                            Design some buttons
                                                            <small class="pull-right">20%</small>
                                                        </h3>
                                                        <!-- The progress bar -->
                                                        <div class="progress xs">
                                                            <!-- Change the css width attribute to simulate progress -->
                                                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                                <span class="sr-only">20% Complete</span>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <!-- end task item -->
                                            </ul>
                                        </li>
                                        
                                    </ul>
                                </li>
                                <!-- User Account Menu -->
                                <li class="dropdown user user-menu">
                                    @if(Auth::check())
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        @if(Auth::user()->profile_picture == null)
                                        <img src="{{asset("images/user.png")}}" class="user-image" alt="User Image">
                                        @else
                                        <img src="{{asset('images/profile/'.Auth::user()->profile_picture)}}" class="user-image" alt="User Image">
                                        @endif
                                        <span class="hidden-xs">{{ auth()->user()->name}}</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <!-- The user image in the menu -->
                                        <li class="user-header">
                                            @if(Auth::user()->profile_picture == null)
                                            <img src="{{asset("images/user.png")}}" class="img-circle" alt="User Image">
                                            @else
                                            <img src="{{asset('images/profile/'.Auth::user()->profile_picture)}}" class="img-circle" alt="User Image">
                                            @endif
                                            <p>
                                                {{ auth()->user()->name}} 
                                                <small>Member since  : {{ auth()->user()->created_at->format('F d, Y') }}</small>
                                            </p>
                                        </li>
                                        
                                        <!-- Menu Footer-->
                                        <li class="user-footer">
                                            <div class="pull-left">
                                                <a href="{{url('profile')}}" class="btn btn-default btn-flat">
                                                    <i class="fa fa-user-circle-o" aria-hidden="true"></i> Profile</a>
                                                </div>
                                                <div class="pull-right">
                                                    <a href="{{ route('logout') }}" class="btn btn-default btn-flat" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                                    <i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                                </div>
                                            </li>
                                        </ul>
                                        @endif
                                    </li>
                                    <!-- Control Sidebar Toggle Button -->
                                    <li>
                                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </header>
                    <!-- Left side column. contains the logo and sidebar -->
                    <aside class="main-sidebar">
                        <section class="sidebar">
                            <ul class="sidebar-menu" data-widget="tree">
                                @can('Main Menu')
                                <li class="header" style="color: lightblue">MAIN MENU</li>
                                <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
                                    <a href="{{url('dashboard')}}"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a>
                                </li>
                                <li class="{{ Request::is('attendance') ? 'active' : '' }}">
                                    <a href="{{url('attendance')}}"><i class="fa fa-clock-o"></i> <span>Attendance</span></a>
                                </li>
                                <li class="{{ Request::is('employee-performance') ? 'active' : '' }}">
                                    <a href="{{url('employee-performance')}}"><i class="fa fa-line-chart"></i> <span>Employee Performance</span></a>
                                </li>
                                <li class="treeview {{ Request::is('*SubmissionForm*') ? 'active' : '' }}">
                                    <a href="#">
                                        <i class="fa fa-wpforms"></i> 
                                        <span>Submission Form</span>
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li  class="{{ Request::is('SubmissionForm-DayOffSubmission') ? 'active' : '' }}">
                                            <a href="{{url('SubmissionForm-DayOffSubmission')}}"><i class="fa fa-dot-circle-o"></i>Day Off Submission</a>
                                        </li>
                                        <li  class="{{ Request::is('SubmissionForm-OvertimeSubmission') ? 'active' : '' }}">
                                            <a href="{{url('SubmissionForm-OvertimeSubmission')}}"><i class="fa fa-dot-circle-o"></i>Overtime Submission</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="treeview {{ Request::is('*Reporting*') ? 'active' : '' }}">
                                    <a href="#"><i class="fa fa-file"></i> <span>Reporting & Summary</span>
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class=" {{ Request::is('Reporting-DayOffReport') ? 'active' : '' }}">
                                            <a href="{{url('Reporting-DayOffReport')}}"><i class="fa fa-dot-circle-o"></i>Day off Report</a>
                                        </li>
                                        <li class=" {{ Request::is('Reporting-OvertimeReport') ? 'active' : '' }}">
                                            <a href="{{url('Reporting-OvertimeReport')}}"><i class="fa fa-dot-circle-o"></i>Overtime Report</a>
                                        </li>
                                        <li class=" {{ Request::is('Reporting-PaySlipReport') ? 'active' : '' }}">
                                            <a href="{{url('Reporting-PaySlipReport')}}"><i class="fa fa-dot-circle-o"></i>Pay Slip Report</a>
                                        </li>
                                    </ul>
                                </li>   
                                @endcan
                                @can('Employee Data')
                                <li class="{{ Request::is('data-employee') ? 'active' : '' }}">
                                    <a href="{{url("data-employee")}}"><i class="fa fa-user-o"></i> <span>Employee Data</span></a>
                                </li>
                                @endcan
                                @can('Employee Payroll')
                                <li>
                                    <a href="#"><i class="fa fa-money"></i> <span>Employee Payroll</span></a>
                                </li>
                                @endcan
                                @can('Employee Report')
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-file"></i> <span>Employee Report</span>
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Attendance</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Salary</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Days Off</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Overtime</a></li>
                                    </ul>
                                </li>
                                @endcan
                                @can('Approval')
                                <li class="{{ Request::is('approval') ? 'active' : '' }}">
                                    <a href="{{url('approval')}}"><i class="fa fa-check-circle"></i> <span>Approval</span>
                                    </a>                             
                                </li>
                                @endcan
                                @hasanyrole('HRD|IT Administrator')
                                <li class="header"> <b style="color: lightblue;">MASTER DATA</b> </li>
                                @endhasrole  
                                {{-- <li class=""><a href="{{route ('permissions.index')}}"><i class="fa fa-th-list"></i> <span>Manage Permissions</span></a></li> --}}
                                @can('Employee Master by Admin')
                                <li class="{{ Request::is('users') ? 'active' : '' }}">
                                    <a href="{{route('users.index')}}"><i class="fas fa-users"></i> <span> Employee Master <b style="font-size: 9px">by Admin</b></span></a>
                                </li>
                                @endcan
                                @can('News Master')
                                <li class="{{Request::is('dashboard/create') ? 'active' : '' }}">
                                    <a href="{{route ('dashboard.create')}}"><i class="fa fa-newspaper-o"></i> <span>News Master</span></a>
                                </li>
                                @endcan
                                @can('Employee Master by HRD')
                                <li class="treeview {{ Request::is('*master*') ? 'active' : '' }}">
                                    <a href="#"><i class="fas fa-users"></i><span>   Employee Master <b style="font-size: 9px">by HRD</b></span>
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        @can('Manage Department')
                                        <li class="{{ Request::is('master-department') ? 'active' : '' }}">
                                            <a href="{{url("master-department")}}"><i class="fa fa-circle-o"></i>Department Master</a>
                                        </li>
                                        @endcan
                                        @can('Manage Division')
                                        <li class="{{ Request::is('master-division') ? 'active' : '' }}">
                                            <a href="{{url("master-division")}}"><i class="fa fa-circle-o"></i>Division Master</a>
                                        </li>
                                        @endcan
                                        @can('Manage Position')
                                        <li class="{{ Request::is('master-position') ? 'active' : '' }}">
                                            <a href="{{url("master-position")}}"><i class="fa fa-circle-o"></i>Position Master</a>
                                        </li>
                                        @endcan
                                        @can('Employee Master by HRD')
                                        <li class="{{ Request::is('master-employee-detail') ? 'active' : '' }}">
                                            <a href="{{url("master-employee-detail")}}"><i class="fa fa-circle-o"></i>Employee Detail</a>
                                        </li>
                                        @endcan
                                    </ul>
                                </li>
                                @endcan
                                @can('Payroll Master')
                                <li class="treeview {{ Request::is('*masterdata-payrollmaster*') ? 'active' : '' }}">
                                    <a href="#"><i class="fa fa-money"></i><span>Payroll Master</span>
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class="{{ Request::is('masterdata-payrollmaster-salarylevelmaster') ? 'active' : '' }}">
                                            <a href="{{url('masterdata-payrollmaster-salarylevelmaster')}}"><i class="fa fa-circle-o"></i>Salary Level Master</a>
                                        </li>
                                        <li class="{{ Request::is('masterdata-payrollmaster-incentivemaster') ? 'active' : '' }}">
                                            <a href="{{ url('masterdata-payrollmaster-incentivemaster') }}"><i class="fa fa-circle-o"></i>Incentive Master</a>
                                        </li>
                                        <li class="{{ Request::is('masterdata-payrollmaster-leavetypemaster') ? 'active' : '' }}">
                                            <a href="{{ url('masterdata-payrollmaster-leavetypemaster') }}"><i class="fa fa-circle-o"></i>Leave Type Master</a>
                                        </li>
                                        <li class="{{ Request::is('masterdata-payrollmaster-BPJStypemaster') ? 'active' : '' }}">
                                            <a href="{{ url('masterdata-payrollmaster-BPJStypemaster') }}"><i class="fa fa-circle-o"></i>BPJS Type Master</a>
                                        </li>
                                        <li class="{{ Request::is('masterdata-payrollmaster-PPHtypemaster') ? 'active' : '' }}">
                                            <a href="{{ url('masterdata-payrollmaster-PPHtypemaster') }}"><i class="fa fa-circle-o"></i>PPH Type Master</a>
                                        </li>
                                        <li class="{{ Request::is('masterdata-payrollmaster-PTKPtypemaster') ? 'active' : '' }}">
                                            <a href="{{ url('masterdata-payrollmaster-PTKPtypemaster') }}"><i class="fa fa-circle-o"></i>PTKP Type Master</a>
                                        </li>
                                        <li class="{{ Request::is('masterdata-payrollmaster-workingdaysmaster') ? 'active' : '' }}">
                                            <a href="{{ url('masterdata-payrollmaster-workingdaysmaster') }}"><i class="fa fa-circle-o"></i>Working Days Master</a>
                                        </li>
                                    </ul>
                                </li>
                                @endcan
                            </ul>
                            <!-- /.sidebar-menu -->
                        </section>
                        <!-- /.sidebar -->
                    </aside>
                    
                    <!-- Content Wrapper. Contains page content -->
                    <div class="content-wrapper" style="padding-bottom: 80px;" >
                        <!-- Content Header (Page header) -->
                        @yield('content')
                        <!-- /.content -->
                    </div>   
                    <footer class="main-footer" style="top: 310px;">
                        <!-- To the right -->
                        <!-- <div class="pull-right hidden-xs">
                            Anything you want
                        </div> -->
                        <!-- Default to the left -->
                        <strong>Copyright &copy; 2019 <a href="{{url('dashboard')}}">Emp<i class="fa fa-user-circle-o" aria-hidden="true"></i>rtal</a>. <b>v</b>1.1.0
                        </footer>
                        <!-- Control Sidebar -->
                        <aside class="control-sidebar control-sidebar-dark">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- Home tab content -->
                                <div class="tab-pane active" id="control-sidebar-home-tab">
                                    <h3 class="control-sidebar-heading">Settings</h3>
                                    <hr>
                                    <ul class="control-sidebar-menu">
                                        <li>
                                            <a href="{{url('update-profile/'.Auth::user()->id)}}">
                                                <i class="menu-icon fa fa-user bg-blue"></i>
                                                <div class="menu-info" style="padding-top: 5%">
                                                    <h4 class="control-sidebar-subheading">Edit Profile</h4>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{url('update-account/'.Auth::user()->id)}}">
                                                <i class="menu-icon fa fa-user bg-blue"></i>
                                                <div class="menu-info" style="padding-top: 5%">
                                                    <h4 class="control-sidebar-subheading">Edit Account</h4>
                                                </div>
                                            </a>
                                        </li>
                                        @can('Administer roles & permissions')
                                        <li>
                                            <a href="{{route ('roles.index')}}">
                                                <i class="menu-icon fa fa-lock bg-blue"></i>
                                                <div class="menu-info" style="padding-top: 5%">
                                                    <h4 class="control-sidebar-subheading">Roles & Permissions</h4>
                                                </div>
                                            </a>
                                        </li>
                                        @endcan
                                        @can('Upload Logo Company')
                                        <li>
                                            <a href="{{url("company-setting")}}">
                                                <i class="menu-icon fa fa-upload bg-blue"></i>
                                                <div class="menu-info" style="padding-top: 5%">
                                                    <h4 class="control-sidebar-subheading">Upload Logo Company</h4>
                                                </div>
                                            </a>
                                        </li>
                                        @endcan
                                    </ul>
                                </div>
                            </div>
                        </aside>
                        <!-- /.control-sidebar -->
                        <!-- Add the sidebar's background. This div must be placed
                            immediately after the control sidebar -->
                            <div class="control-sidebar-bg"></div>
                        </div>
                        <!-- ./wrapper -->
                        
                        <!-- REQUIRED JS SCRIPTS -->
                        
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>   
                        <script src="{{ asset ('dist/js/time.js') }}"></script>
                        {{-- alert toastr --}}
                        <script>
                            @if(Session::has('message'))
                            var type="{{Session::get('alert-type','info')}}"           
                            switch (type) {
                                case 'success':
                                toastr.success("{{ Session::get('message') }}");
                                break;
                                case 'error':
                                toastr.error("{{ Session::get('message') }}");
                                break;
                                case 'info':
                                toastr.info("{{ Session::get('message') }}");
                                break;
                            }
                            @endif
                        </script>
                        
                        <!-- Bootstrap 3.3.7 -->
                        {{-- <script src="{{ asset ('dist/js/clock.js') }}"></script> --}}
                        {{-- <script>
                            $(function () {
                                //Initialize Select2 Elements
                                $('.select2').select2()
                                
                                //Datemask dd/mm/yyyy
                                $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
                                //Datemask2 mm/dd/yyyy
                                $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
                                //Money Euro
                                $('[data-mask]').inputmask()
                                
                                //Date range picker
                                $('#reservation').daterangepicker()
                                //Date range picker with time picker
                                $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
                                //Date range as a button
                                $('#daterange-btn').daterangepicker(
                                {
                                    ranges   : {
                                        'Today'       : [moment(), moment()],
                                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                    },
                                    startDate: moment().subtract(29, 'days'),
                                    endDate  : moment()
                                },
                                function (start, end) {
                                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                                }
                                )
                                
                                //Date picker
                                $('#datepicker').datepicker({
                                    autoclose: true
                                })
                                
                                //iCheck for checkbox and radio inputs
                                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                                    checkboxClass: 'icheckbox_minimal-blue',
                                    radioClass   : 'iradio_minimal-blue'
                                })
                                //Red color scheme for iCheck
                                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                                    checkboxClass: 'icheckbox_minimal-red',
                                    radioClass   : 'iradio_minimal-red'
                                })
                                //Flat red color scheme for iCheck
                                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                                    checkboxClass: 'icheckbox_flat-green',
                                    radioClass   : 'iradio_flat-green'
                                })
                                
                                //Colorpicker
                                $('.my-colorpicker1').colorpicker()
                                //color picker with addon
                                $('.my-colorpicker2').colorpicker()
                                
                                //Timepicker
                                $('.timepicker').timepicker({
                                    showInputs: false
                                })
                            })
                            
                        }
                    </script> --}}
                </body>
                </html>