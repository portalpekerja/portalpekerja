@extends('layouts.app') 
@section('content')


<section class="content-header">
    
    {{-- @can('Manage Department') --}}
    <h1>
        Working Days Master       
    </h1> 
    <ol class="breadcrumb">
        <li><a href="{{url('Dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-money"></i>Payroll Master</a></li>
        <li class="active">Working Days Master</li>
    </ol>
</section>

<br>
{{-- @include('layouts.partials.alerts') --}}
<body>

{{-- spasi buat kebawahin --}}
<br><br><br>

{{-- search bar --}}
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8" >
        <form action="{{url('#')}}" method="POST">
            {{csrf_field()}}
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search by Name">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-secondary">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
    </div>
    <div class="col-md-2"></div>
</div>
<br>

{{-- buat munculin data yang dicari --}}
{{-- <div class="row">
    @if(isset($search))
    <div class="col-md-8 col-md-offset-2" >
        <br>
        <p>Your Search Result with '<b>{{$search}}</b>' Keyword(s)</p>
    </div>
    @endif
</div> --}}
{{-- // --}}

{{-- Box Working Days Master level --}}
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        
        {{-- box primary --}}
        <div class="box box-primary">  
            <div class="box-header">
                <h3 class="box-title">Working Days Data</h3>
                <a href="#modal-addsalarylevel" data-toggle="modal" class="btn btn-primary  pull-right" style="margin-top: -5px">Add new working days</a>
            </div>

            {{-- box body --}}
            <div class="box-body">

                {{-- table salary level --}}
                <table  class="table table-bordered table-striped" >
                    <thead>
                        <tr>
                            <th style="text-align: center"><b>No</b></th>
                            <th style="text-align: center"><b>Level Name</b></th>     
                            <th style="text-align: center"><b>Salary</b></th>     
                            <th style="text-align: center"><b>Year</b></th>     
                            <th style="text-align: center"><b>Edit/Delete</b></th>             
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="text-align: center">[no]</td>
                            <td style="text-align: center">[level 1]</td>
                            <td style="text-align: center">[salary]</td>
                            <td style="text-align: center">[year]</td>
                            {{-- tools [edit-delete] --}}
                            <td style="text-align: center">
                                <div class="tools">
                                    {{-- <a href="#" onclick="departmentFormEdit()" data-toggle="tooltip" title="Edit" style="margin-right:5px;"><i class="fa fa-edit"></i></a> --}}

                                    {{-- edit&delete belum dikasih id --}}
                                    <a href="#modal-salarylevelEdit" title="Edit" data-toggle="tooltip" style="margin-right:5px;"><i class="fa fa-edit"></i></a>
                                    <a href="#"  data-toggle="tooltip" title="Delete" style="margin-right: 5px;"><i class="fa fa-trash"></i></a> 
                                </div>
                            </td>
                        </tr> 
                    </tbody>
                </table>
                {{-- /table salary level --}}
            </div>
            {{-- /box body --}}

        {{-- box footer --}}
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&raquo;</a></li>
                </ul>
            </div>
            {{-- /box footer --}}
            
        </div>
        {{-- /box primary --}}

    </div>   
    <div class="col-md-2"></div>
</div>   

{{-- modal fix Add new working days pop-up--}}
<div class="modal fade" id="modal-addsalarylevel" tabindex="1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" >
                
                {{-- token --}}
                {{csrf_field()}}   
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title">Add new working days</h3>
                </div>    
                
                <div class="modal-body">
                
                        {{-- token --}}
                        {{csrf_field()}}

                        {{-- Tahun  --}}
                        <div class="form-group" style="margin: 20px">
                            <label for="">Year</label>
                            <input type="number" name="" class="form-control" value="2019">
                        </div>
                        
                        {{-- Bulan --}}
                        {{-- <div class="form-group" >
                            <label for="">Month</label>
                            <input type="text" name="" class="form-control" placeholder="-input Month-">
                        </div> --}}

                        <div class="form-group" style="margin: 20px">
                            <label>Month</label>
                            <select class="form-control" style="width: 100%;">
                                <option selected="selected">--Month--</option>
                                <option>January</option>
                                <option>February</option>
                                <option>March</option>
                                <option>April</option>
                                <option>May</option>
                                <option>June</option>
                                <option>July</option>
                                <option>August</option>
                                <option>September</option>
                                <option>October</option>
                                <option>November</option>
                                <option>December</option>
                            </select>
                        </div>
                        
                        {{-- Total Hari --}}
                        <div class="form-group" style="margin: 20px">
                            <label for="">Number of Day(s)</label>
                            <input type="number" name="" class="form-control" value="0">
                        </div>

                        {{-- Jumlah hari kerja --}}
                        <div class="form-group" style="margin: 20px">
                            <label for="">Number of working Days</label>
                            <input type="number" name="" class="form-control" value="0">
                        </div>

                        {{-- Jumlah jam kerja(hari) --}}
                        <div class="form-group" style="margin: 20px">
                            <label for="">Number of working Hour</label>
                            <input type="number" name="" class="form-control" value="0">
                        </div>

                        {{-- Jumlah jam kerja(hari) --}}
                        <div class="form-group" style="margin: 20px">
                            <label for="">Number of day off</label>
                            <input type="number" name="" class="form-control" value="0">
                        </div>
                        
                        {{-- catatan buat bulan dan tahun yang diinput sudah ada | perlu dikasih validasi--}}
                        <div class="form-group" style="margin: 20px">
                            <label>Notes : <span style="color:red">[number of working days for [month] [year] already exist]</span></label>
                        </div>
                                
                </div>
                <div class="modal-footer">
                    {{-- button --}}
                    <button type="submit" class="btn btn-primary btn-save">Add</button>
                    <button type="submit" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    
                </div>

            </form>
        </div>
    </div>
</div> 


{{-- modal edit departmnt --}}

{{-- </div> 
<script src="{{ asset ('dist/js/sweetalert2.all.min.js') }}"></script>
<script>
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});
    
</script> --}}

</body>
@endsection