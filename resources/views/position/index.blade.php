        @extends('layouts.app') 
        @section('content')
        
        
        <section class="content-header">
            
            @can('Manage Position')
            Master Position
            
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('Dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#"><i class="fa fa-users"></i> Manage Position</a></li>
            <li class="active">Master Position</li>
        </ol>
    </section>
    
    <br>
    @include('layouts.partials.alerts')
    
    
    <br><br><br>
    <div class="row">
        <div class="col-md-8 col-md-offset-2" >
            <form action="{{url('position-search')}}" method="POST">
                {{csrf_field()}}
                <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="Search by Name">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-secondary">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
    <br>
    <div class="row">
        @if(isset($search))
        <div class="col-md-8 col-md-offset-2" >
            <br>
            <p>Your Search Result with '<b>{{$search}}</b>' Keyword(s)</p>
        </div>
        @endif
    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2" style="bottom: 30px">
            
            <br><br> 
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data Position</h3>
                    <a href="#" onclick="positionForm()" class="btn btn-primary  pull-right" style="margin-top: -5px">Add Position</a>
                    
                </div>
                
                
                <table  class="table table-bordered table-striped" >
                    <thead>
                        <tr>
                            <th><b style="left:20px;position: relative">No</b></th>
                            <th ><b style="left: 100px;position: relative">Name</b> </th>     
                            <th>Operation</th>             
                        </tr>
                    </thead>
                    <tbody>
                    </tfoot>
                    
                    <?php $no = 0?>
                    @foreach ($position as $p)
                    <?php $no++?>
                    <tr>
                        <td><b style="left:25px;position: relative">{{$no}}</b></td>
                        <td ><b style="left: 100px;position: relative">{{ $p->name }}</b></td>
                        <td>
                            <div class="tools">
                                
                                {{-- <a href="#" onclick="departmentFormEdit()" data-toggle="tooltip" title="Edit" style="margin-right:5px;"><i class="fa fa-edit"></i></a> --}}
                                <span data-toggle="tooltip" data-target="#id" title="Edit">
                                    <a href="#modal-positionEdit-{{$p->id}}" data-toggle="modal" style="margin-right:5px;"><i class="fa fa-edit"></i></a>
                                </span>
                                <a href="{{url("delete-position/$p->id")}}"  data-toggle="tooltip" title="Delete" style="margin-right: 5px;"><i class="fa fa-trash"></i></a> 
                            </div>
                        </td>
                    </tr>
                    <div class="modal fade" id="modal-positionEdit-{{$p->id}}" tabindex="1" role="dialog" data-backdrop="static" aria-hidden="true">
                        <div class="modal-dialog" modal-lg>
                            <div class="modal-content">
                                <form action="{{url('edit-position/'.$p->id)}}" method="post" class="form-horizontal" data-toggle="validator" >
                                    {{csrf_field()}}   
                                    
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="modal-title">Edit  Position</h3>
                                    </div>    
                                    
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="" class="col-md-3 control-label">Name</label>
                                            <div class="col-md-6">
                                                <input type="text" name="name" class="form-control"  value="{{ $p->name }}">
                                            </div>
                                            
                                        </div>        
                                    </div>
                                    <div class="modal-footer">
                                        
                                        <button type="submit" class="btn btn-primary btn-save">Save</button>
                                        <button type="submit" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> 
                    @endforeach
                </tfoot>
            </table>
            
            
        </div>
    </div>   
    
    {{-- modal add departmnt --}}
    <div class="modal fade" id="modal-position" tabindex="1" role="dialog" data-backdrop="static" aria-hidden="true">
        <div class="modal-dialog" modal-lg>
            <div class="modal-content">
                <form action="{{url('add-position')}}" method="post" class="form-horizontal" data-toggle="validator" >
                    {{csrf_field()}}   
                    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title">Add New Position</h3>
                    </div>    
                    
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="export" class="col-md-3 control-label">Name</label>
                            <div class="col-md-6">
                                <input type="text" name="name" class="form-control">
                            </div>
                            
                        </div>        
                    </div>
                    <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary btn-save">Add</button>
                        <button type="submit" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        
                    </div>
                </form>
            </div>
        </div>
    </div> 
    
    
    {{-- modal edit departmnt --}}
    
</div>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
    
    function positionForm(){
        $('#modal-position').modal('show');
        $('#modal-position form')[0].reset();
    } 
    
    // function departmentFormEdit(){
        //         $('#modal-departmentEdit').modal('show');
        //         $('#modal-department form')[0].reset();
        // } 
        
    </script>
    @endcan
    @endsection