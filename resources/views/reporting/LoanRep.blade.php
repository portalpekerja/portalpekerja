@extends('layouts.app')
@section('content')

        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Loan Report 
            <small>Detail Information</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a></i>Reporting</a></li>
            <li class="active">Loan Report</li>
          </ol>
        </section>
    
        <!-- Main content -->
        <section class="content">
            <br>
            <br>
          <div class="row">
            <div class="col-md-3">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-cart-plus" aria-hidden="true"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Total Loan Item</span>
                  <span class="info-box-number">0</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-check" aria-hidden="true"></i></span>
    
                <div class="info-box-content">
                  <span class="info-box-text">Approved</span>
                  <span class="info-box-number">0</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-times" aria-hidden="true"></i></span>
    
                <div class="info-box-content">
                  <span class="info-box-text">Rejected</span>
                  <span class="info-box-number">0</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
    
                <div class="info-box-content">
                  <span class="info-box-text">Waiting</span>
                  <span class="info-box-number">0</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
    
          <!-- =========================================================== -->
    
              <div class="row">
                {{-- status Histori cuti --}}
                <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                      <h3 class="box-title">Loan History</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <table class="table table-bordered">
                        <tr>
                          <th style="width: 10px; text-align: center;">No.</th>
                          <th style="text-align: center;">Loan Item</th>
                          <th style="text-align: center;">Duration(Day)</th>                        
                          <th style="text-align: center;">Start Date</th>
                          <th style="text-align: center;">End Date</th>
                          <th style="text-align: center;">Status</th>
                        </tr>
                        <tr>
                          <td>1.</td>
                          <td>(Day off type)</td>
                          <td>(Duration)</td>
                          <td>(Start Date)</td>
                          <td>(End Date)</td>
                          <td style="text-align: center;"><span class="label label-success">Approved</span></td>
                        </tr>
                        <tr>
                          <td>2.</td>
                          <td>(Day off type)</td>
                          <td>(Duration)</td>
                          <td>(Start Date)</td>
                          <td>(End Date)</td>
                          <td style="text-align: center;"><span class="label label-warning">Waiting</span></td>
                        </tr>
                        <tr>
                          <td>3.</td>
                          <td>(Day off type)</td>
                          <td>(Duration)</td>
                          <td>(Start Date)</td>
                          <td>(End Date)</td>
                          <td style="text-align: center;"><span class="label label-warning">Waiting</span></td>
                        </tr>
                        <tr>
                          <td>4.</td>
                          <td>(Day off type)</td>
                          <td>(Duration)</td>
                          <td>(Start Date)</td>
                          <td>(End Date)</td>
                          
                          <td style="text-align: center;"><span class="label label-warning">Waiting</span></td>
                        </tr>
                      </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                      <ul class="pagination pagination-sm no-margin pull-right">
                        <li><a href="#">&laquo;</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">&raquo;</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
         </section>
@endsection
