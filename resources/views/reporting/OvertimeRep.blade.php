@extends('layouts.app')
@section('content')

<section>
  <div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
      <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="0" /> 
    </svg>
  </div>
  <body class="fix-header fix-sidebar card-no-border">
    <div id="main-wrapper">
        <div class="content-wrapper">
          <!-- ============================================================== -->
          <!-- Container fluid  -->
          <!-- ============================================================== -->
          <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
              <div class="col-md-12 col-8 align-self-center">
                <h3 class="text-themecolor">Overtime Report</h3>
              </div>
              <div class="col-md-12 col-8 align-self-center">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item ">Reporting</li>
                  <li class="breadcrumb-item ">Overtime Report</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-8">
          <div class="card">
            <div class="card-body">
              <div class="d-flex flex-row">
                <div class="round round-lg align-self-center round-info"><i class="mdi mdi-clock-out"></i></div>
                <div class="m-l-10 align-self-center">
                  <h3 class="m-b-0 font-light">5</h3>
                  <h5 class="text-muted m-b-0">Overtime Total</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-8">
          <div class="card">
            <div class="card-body">
              <div class="d-flex flex-row">
                <div class="round round-lg align-self-center round-success"><i
                    class="mdi mdi-checkbox-multiple-marked"></i>
                </div>
                <div class="m-l-10 align-self-center">
                  <h3 class="m-b-0 font-light">5</h3>
                  <h5 class="text-muted m-b-0">Approved</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="d-flex flex-row">
                <div class="round round-lg align-self-center round-danger">
                  <i class="mdi mdi-close-circle-outline"></i>
                </div>
                <div class="m-l-10 align-self-center">
                  <h3 class="m-b-0 font-lgiht"> 3</h3>
                  <h5 class="text-muted m-b-0">Rejected </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="d-flex flex-row">
                <div class="round round-lg align-self-center round-warning">
                  <i class="fas fa-info-circle"></i>
                </div>
                <div class="m-l-10 align-self-center">
                  <h3 class="m-b-0 font-lgiht"> 1</h3>
                  <h5 class="text-muted m-b-0">Pending</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="card">
          <div class="card-body">
              <h4 class="card-title">Day off History</h4>
              <div class="table-responsive m-t-40">
                <table id="myTable" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Date</th>
                      <th>Start Time</th>
                      <th>End Time</th>
                      <th>Hours</th>
                      <th>Reason</th>
                      <th>Assign to Person</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>15-03-2019</td>
                      <td>17:00</td>
                      <td>21:00</td>
                      <td>4 hours</td>
                      <td>Ingin menyelesaikan bagian yang belum hari ini</td>
                      <td>Halim Cakra W</td>
                      <td><span class="label label-table label-warning">Pending</span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
        </div>
      </div>
    </body>
    <script>
        $(function () {
            $('#myTable').DataTable();
            $(function () {
            var table = $('#example').DataTable({
            "columnDefs": [{
            "visible": false,
            "targets": 2
            }],
            "order": [
            [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function (settings) {
            var api = this.api();
            var rows = api.rows({
            page: 'current'
            }).nodes();
            var last = null;
            api.column(2, {
            page: 'current'
            }).data().each(function (group, i) {
            if (last !== group) {
            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
            last = group;
            }
            });
            }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
            table.order([2, 'desc']).draw();
            } else {
            table.order([2, 'asc']).draw();
            }
            });
            });
            });
            $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'csv', 'excel', 'pdf'
            ]
            });
            function eximForm(){
            $('#modal-exim').modal('show');
            $('#modal-exim form')[0].reset();
        } 
    </script>
</section>

@endsection