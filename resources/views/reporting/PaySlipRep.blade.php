@extends('layouts.app')
@section('content')

        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Pay Slip Report 
            <small>Detail Information</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a></i>Reporting</a></li>
            <li class="active">Pay Slip Report</li>
          </ol>
        </section>
    
        <!-- Main content -->
        <section class="content">     
           <br>
           <br>
                {{-- row 1 --}}
                <div class="row">
                    <div class="col-md-4">
            
                        <!-- Simple Employee Profile  -->
                        <div class="box box-primary">
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">
            
                            <h3 class="profile-username text-center">Employee Name</h3>
            
                            <p class="text-muted text-center">Department</p>
                            <p class="text-muted text-center">Division</p>
                            <p class="text-muted text-center">Position</p>
            
                            <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Company location</b> <a class="pull-right">Location</a>
                            </li>
                            <li class="list-group-item">
                                <b>Email</b> <a class="pull-right">admin@portalpekerja.com</a>
                            </li>
                            <li class="list-group-item">
                                <b>Phone Number</b> <a class="pull-right">(+62) 81310632724</a>
                            </li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                        </div>
                    </div>
                    {{-- /col-md-4 --}}

                    <div class="col-md-8">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                            <li class="active"><a href="#SalaryDetail" data-toggle="tab">Salary</a></li>
                            <li><a href="#PaySlip" data-toggle="tab">Pay Slip</a></li>
                            </ul>
                            <div class="tab-content">

                            {{-- Salary Detail --}}
                            <div class="active tab-pane" id="SalaryDetail">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Salary Detail</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <table class="table table-bordered">
                                        <tr>
                                            <th style="width: 10px; text-align: center;">No.</th>
                                            <th style="text-align: center;">Salary Component</th>
                                            <th style="text-align: center;">Rate</th>                        
                                        </tr>
                                        <tr>
                                            <td>1.</td>
                                            <td>(Component)</td>
                                            <td>(IDR 000)</td>
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>(Component)</td>
                                            <td>(IDR 000)</td>
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>(Component)</td>
                                            <td>(IDR 000)</td>
                                        </tr>
                                        <tr>
                                            <td>4.</td>
                                            <td>(Component)</td>
                                            <td>(IDR 000)</td>
                                        </tr>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer clearfix">
                                        <ul class="pagination pagination-sm no-margin pull-right">
                                        <li><a href="#">&laquo;</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">&raquo;</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /Salary Detail -->

                            {{-- Pay Slip --}}
                            <div class="tab-pane" id="PaySlip">
                                    <div class="box">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Pay Slip Detail</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <table class="table table-bordered">
                                                <tr>
                                                    <th style="width: 10px; text-align: center;">No.</th>
                                                    <th style="text-align: center;">Month</th>
                                                    <th style="text-align: center;">Print Date</th>
                                                    <th style="text-align: center;">button</th>                        
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center">1.</td>
                                                    <td style="text-align: center">(Month)</td>
                                                    <td style="text-align: center">(Date)</td>
                                                    <td style="text-align: center"><a href="{{url('Reporting-PaySlipReport-PaySlipDetail')}}"><button class="btn btn-default" type="button">Detail</button></a></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center">2.</td>
                                                    <td style="text-align: center">(Month)</td>
                                                    <td style="text-align: center">(Date)</td>
                                                    <td style="text-align: center"><a href="{{url('Reporting-PaySlipReport-PaySlipDetail')}}"><button class="btn btn-default" type="button">Detail</button></a></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center">3.</td>
                                                    <td style="text-align: center">(Month)</td>
                                                    <td style="text-align: center">(Date)</td>
                                                    <td style="text-align: center"><a href="{{url('Reporting-PaySlipReport-PaySlipDetail')}}"><button class="btn btn-default" type="button">Detail</button></a></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center">4.</td>
                                                    <td style="text-align: center">(Month)</td>
                                                    <td style="text-align: center">(Date)</td>
                                                    <td style="text-align: center"><a href="{{url('Reporting-PaySlipReport-PaySlipDetail')}}"><button class="btn btn-default" type="button">Detail</button></a></td>
                                                </tr>
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                            <div class="box-footer clearfix">
                                                <ul class="pagination pagination-sm no-margin pull-right">
                                                <li><a href="#">&laquo;</a></li>
                                                <li><a href="#">1</a></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">&raquo;</a></li>
                                                </ul>
                                            </div>
                                        </div>
                            </div>
                            <!-- /Pay Slip-->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- /.nav-tabs-custom -->
                    </div>

                </div>
                {{-- /row 1 --}}

         </section>
@endsection
