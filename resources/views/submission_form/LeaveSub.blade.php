@extends('layouts.app')
@section('content')

<section class="content">
  <div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
      <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="0" /> </svg>
  </div>

  <body class="fix-header fix-sidebar card-no-border">
    <div id="main-wrapper">
      <div class="content-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
          <!-- ============================================================== -->
          <!-- Bread crumb and right sidebar toggle -->
          <!-- ============================================================== -->
          <div class="row page-titles">
            <div class="col-md-12 col-8 align-self-center">
              <h3 class="text-themecolor">Submission Form</h3>
            </div>
            <div class="col-md-12 col-8 align-self-center">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Submission Form</li>
                <li class="breadcrumb-item active">Days Off Submissions</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
        @if(!$errors->isEmpty())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>{{$errors->first()}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title" style="text-align: center">Days Off Submissions</h4>
            <hr>

            <form action="{{url('SubmissionForm-leaveSubmission-store')}}" method="post" class="floating-labels m-t-40 ">
                @csrf
              <div class="form-group m-b-40">
                <select name="leave_type" class="form-control p-0" id="input6" value="{{old('leave_type')}}" onchange="changeMaxDay(this);">
                    @foreach($leave_types as $data)
                        <option value="{{$data->id}}" data-lenght="{{$data->day_amount}}">{{$data->leave_type_name}}</option>
                    @endforeach
                </select><span class="bar"></span>
                <label for="input6">Leave Type</label>
              </div>
              <div class="form-group m-b-5">
                <input type="text" class="form-control" id="min-date">
                <span class="bar"></span>
                <label for="input8">Start date to End date</label>
              </div>
                <br><br>
                <input type="hidden" name="start_date" value="{{old('start_date')}}">
                <input type="hidden" name="end_date" value="{{old('end_date')}}">
                <label class="form-group m-b-5">Leave Durations</label>
                <div class="input-group" id="wrap_leave_duration">
                    <input name="leave_duration" class="form-control" value="{{old('leave_duration')}}" disabled>
                    <span class="input-group-btn">
                  <div class="input-group-append">
                    <button class="btn btn-success" type="button" onclick="checkLeave(this)">Check</button>
                  </div>
                </span>
                </div>
              <br>
              <div class="form-group m-b-5">
                <textarea class="form-control" rows="4" id="input7" name="reason">{{old('reason')}}</textarea>
                <span class="bar"></span>
                <label for="input7">Reasons</label>
              </div>
              <br>
              <button class="btn btn-success" type="submit">SUBMIT</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <script>
        var range = 0, isCustomLeave = true, leave_type_id = 1;
        function changeMaxDay(e) {
            var selected = $(e).val();
            if (e.value === '1') isCustomLeave = true;
            else isCustomLeave = false;
            leave_type_id = e.value;
            maxDays = $('select[name="leave_type"] option[value="'+selected+'"]').data('lenght');
        }
        $('#min-date').dateRangePicker({
            autoClose: true,
            startDate: new Date(),
            showShortcuts: false,
            disabledDays: [1],
        }).bind('datepicker-change',function(event,obj) {
            var start = moment(obj.date1),
                end = moment(obj.date2);
            range = end.diff(start, 'days')+1;
            $('input[name="leave_duration"]').val(range);
            $('input[name="start_date"]').val(moment(obj.date1));
            $('input[name="end_date"]').val(moment(obj.date2));
            $('#min-date').val(obj.value);
        });

        function checkLeave() {
            if(isCustomLeave === false) swal('Leave Type','Please check you leave type', 'info');
            else {
                send_check_leave('leaveSub-check', leave_type_id, range);
            }
        }
        function send_check_leave(url, type, duration) {
            $.ajax({
                url : url,
                method : 'post',
                data : {
                    _token : $('meta[name="csrf-token"]').attr('content'),
                    leave_type_id : type,
                    durasi : duration,
                    start_date : $('input[name="start_date"]').val(),
                    end_date : $('input[name="end_date"]').val(),
                },
                success: function (response) {
                    $('input[name="leave_duration"]').val(response.diff);
                    swal(response['alert-type']+'',response.message+'',response['alert-type']);
                },
                error: function (data) {
                    var error = data.responseJSON;
                    $('input[name="leave_duration"]').val(error.diff);
                    swal(error['alert-type']+'',error.message+'',error['alert-type']);
                }
            })
        }
    </script>
  </body>
</section>


@endsection
