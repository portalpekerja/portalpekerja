@extends('layouts.app')
@section('content')

        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Loan Submission 
            <small>Add New Submission</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a></i>Submission Form</a></li>         
            <li class="active">Loan Submission</li>
          </ol>
        </section>
    
        <!-- Main content -->
        <section class="content">
            <br>
            <br>
          <div class="row">  
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header">
                      <h3 class="box-title">Submission</h3>
                    </div>                
                    
                    <form role="form">
                      <div class="box-body">

                        {{-- nama peminjam (AUTO GENERATE) --}}
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" placeholder="Name(Auto Generate)">
                        </div>

                        {{-- Nomor induk pegawai --}}
                        <div class="form-group">
                            <label>Employee ID Number</label>
                            <input type="text" class="form-control" placeholder="NIP(Auto Generate)">
                        </div>

                        {{-- nama departemen --}}
                        <div class="form-group">
                            <label>Department</label>
                            <input type="text" class="form-control" placeholder="Department(Auto Generate)">
                        </div>
                        
                        {{-- nama divisi --}}
                        <div class="form-group">
                            <label>Division</label>
                            <input type="text" class="form-control" placeholder="Division(Auto Generate)">
                        </div>
                        
                        {{-- nama posisi --}}
                        <div class="form-group">
                            <label>Position</label>
                            <input type="text" class="form-control" placeholder="Position(Auto Generate)">
                        </div>
                        
                        {{-- nama barang --}}
                        <div class="form-group">
                            <label>Item Name</label>
                            <select class="form-control" style="width: 100%;">
                              <option selected="selected">--Item--</option>
                              <option>option 1</option>
                              <option>option 2</option>
                              <option>option 3</option>
                              <option>option 4</option>
                              <option>option 5</option>
                            </select>
                        </div>

                        {{-- Pinjama start date--}}
                        <div class="form-group">
                            <label>Loan Start Date:</label>
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="datepicker">
                            </div>
                        </div>
                        
                        {{-- pinjaman end date --}}
                        <div class="form-group">
                            <label>Loan End Date:</label>
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="datepicker2">
                            </div>
                        </div>
                        
                        {{-- button submit --}}
                        <button class="btn btn-primary" type="submit">SUBMIT</button>

                      </div>
                      <!-- /.box-body -->
                    </form>
                    {{-- form --}}
                  </div>
            </div>
            <div class="col-md-3"></div>
        </div>
        </section>
@endsection
