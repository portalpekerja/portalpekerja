@extends('layouts.app-spinner')
@section('content')

<section class="content">
  <body class="fix-header fix-sidebar card-no-border">
    <div id="main-wrapper">
      <div class="content-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
          <!-- ============================================================== -->
          <!-- Bread crumb and right sidebar toggle -->
          <!-- ============================================================== -->
          <div class="row page-titles">
            <div class="col-md-12 col-8 align-self-center">
              <h3 class="text-themecolor">Submission Form</h3>
            </div>
            <div class="col-md-12 col-8 align-self-center">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Submission Form</li>
                <li class="breadcrumb-item active">Overtime Submissions</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Overtime Submissions</h4>
                    <hr>
                    <form class="form p-t-20">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">
                                        <i class="ti-calendar"></i>
                                    </span>
                                </div>
                                <input   type="text"  id="min-date" class="form-control"placeholder="Enter date">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pwd1">Start Time</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">
                                        <i class="mdi mdi-clock-start"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" id="timepicker" placeholder="Enter start time">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pwd2">End Time</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">
                                        <i class="mdi mdi-clock-end"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" id="timepicker1" placeholder="Enter end time">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Reason</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">
                                        <i class="mdi mdi-information-outline"></i>
                                    </span>
                                </div>
                                <input   type="text"  class="form-control" placeholder="Enter reason">
                            </div>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    <script>
      // MAterial Date picker
        $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY ', minDate: new Date() });
        $('#min-date1').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY ', minDate: new Date() });
        $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
        $('#timepicker1').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
        $('.datetime').daterangepicker({
                    timePicker: true,
                    timePickerIncrement: 30,
                    locale: {
                        format: 'MM/DD/YYYY h:mm A'
                    }
        });
        $('.timeseconds').daterangepicker({
                    timePicker: true,
                    timePickerIncrement: 30,
                    timePicker24Hour: true,
                    timePickerSeconds: true,
                    locale: {
                        format: 'MM-DD-YYYY h:mm:ss'
                    }
        });
        $('.input-daterange-timepicker').daterangepicker({
                    timePicker: true,
                    format: 'MM/DD/YYYY h:mm A',
                    timePickerIncrement: 30,
                    timePicker12Hour: true,
                    timePickerSeconds: false,
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-danger',
                    cancelClass: 'btn-inverse'
        });

    </script>
  </body>
</section>


@endsection