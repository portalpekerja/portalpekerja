<?php
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', 'HomeController@index')->name('home');

//  INI ROUTE UNTUK DI MASTER DATA

// route employee_master by admin
Route::resource('users', 'UserController');
Route::get('delete-user/{id}', 'UserController@deleteUser');
Route::get('export-userAdmin', 'UserController@userExportAdmin');
Route::post('import-userAdmin', 'UserController@userImportAdmin');
Route::get('download-template-admin', 'UserController@downloadtemplateAdmin');


// route untuk employe_master by hrd
Route::get('master-employee-detail', 'UserController@dataEmployee1');
Route::get('edit-employee/{id}', 'UserController@editEmployee');
Route::post('edit-employee/{id}', 'UserController@doEditEmployee');
Route::get('download-template-hrd', 'UserController@downloadtemplateHrd');

// route user
Route::get('profile', 'UserController@profile');
Route::post('users-search', 'UserController@searchuser');
Route::get('users-detail/{id}', 'UserController@detail')->name('users.detail');;



// INI ROUTE UNTUK DI MAIN MENU
// employee data
Route::get('data-employee', 'UserController@dataEmployee');



//  INI DI SETTINGS
//  update account profile
Route::get('update-account/{id}', 'UserController@updateAccount');
Route::post('update-account/{id}', 'UserController@doUpdateAccount');
Route::get('update-profile/{id}', 'UserController@updateProfile');
Route::post('update-profile/{id}', 'UserController@doUpdateProfile');

//  update and remove image
Route::post('user-update-picture', 'UserController@updatepicture');
Route::get('user-remove-picture/{id}', 'UserController@removepicture');

//  route roles
Route::resource('roles', 'RoleController');
Route::get('delete-role/{id}', 'RoleController@deleteRole');

// 
// route permission
Route::resource('permissions', 'PermissionController');

// route dashboard
Route::resource('dashboard', 'DashboardController');

// route todo list
Route::post('add-todo', 'DashboardController@addtodo');
Route::get('add-todo', function () {
    return view('dashboard');
});
Route::get('delete-todo/{id}', 'DashboardController@deletetodo');
// route attendance
Route::get('attendance', 'AbsenController@index');
Route::post('do_absen', 'AbsenController@doabsen'); 
// route employee performance
Route::get('employee-performance', 'EmployeePerformance@index');
// route import export add user
Route::get('export-user', 'UserController@userExport');
Route::post('import-user', 'UserController@userImport');
Route::get('download-template-user', 'UserController@downloadtemplate');
// route donwload template file user
route::get('donwload-files', 'DashboardController@donwloadFiles');
// route untuk master department
Route::get('master-department', 'DepartmentController@manageDepartment');
Route::post('department-search', 'DepartmentController@searchdepartment');

// 
Route::post('add-department', 'DepartmentController@adddepartment');
Route::get('add-department', function () {
    return view('department.add');
});
Route::get('delete-department/{id}', 'DepartmentController@deleteDepartment');
Route::get('edit-department/{id}', 'DepartmentController@editdepartment');
Route::post('edit-department/{id}', 'DepartmentController@doeditdepartment');

// route master division
Route::get('master-division', 'DivisionController@manageDivision');
Route::post('division-search', 'DivisionController@searchdivision');
Route::post('add-division', 'DivisionController@adddivision');
Route::get('add-division', function () {
    return view('division.index');
});
Route::get('delete-division/{id}', 'DivisionController@deleteDivision');
Route::get('edit-division/{id}', 'DivisionController@editdivision');
Route::post('edit-division/{id}', 'DivisionController@doeditdivision');

// route master position
Route::get('master-position', 'PositionController@managePosition');
Route::post('position-search', 'PositionController@searchposition');
Route::post('add-position', 'PositionController@addposition');
Route::get('add-position', function () {
    return view('position.index');
});
Route::get('delete-position/{id}', 'PositionController@deletePosition');
Route::get('edit-position/{id}', 'PositionController@editposition');
Route::post('edit-position/{id}', 'PositionController@doeditposition');


// route Submission form

// leave
Route::get('SubmissionForm-DayOffSubmission', 'SubmissionFormController@leaveSubindex');
Route::post('SubmissionForm-DayOffSubmission', 'SubmissionFormController@addDoLeaveType');
Route::get('leave-duration', 'SubmissionFormController@FindLTDuration');
Route::post('leaveSub-check', 'SubmissionFormController@leaveSubCheck');
Route::post('SubmissionForm-leaveSubmission-store', 'SubmissionFormController@leaveSubmissionStore');

// overtime
Route::get('/SubmissionForm-LoanSubmission', 'SubmissionFormController@loanSubIndex');
Route::get('/SubmissionForm-OvertimeSubmission', 'SubmissionFormController@OvertimeSubIndex');

// route reporting
Route::get('/Reporting-DayOffReport', 'ReportingController@DayOffRepindex');
Route::get('/Reporting-PaySlipReport', 'ReportingController@PaySlipRepindex');
Route::get('/Reporting-PaySlipReport-PaySlipDetail', 'ReportingController@PaySlipDetail');
Route::get('/Reporting-PaySlipReport-PaySlipDetail-Print', 'ReportingController@PaySlipPrint');
Route::get('/Reporting-OvertimeReport', 'ReportingController@OvertimeRepindex');
Route::get('/Reporting-LoanReport', 'ReportingController@LoanRepindex');


//  route Company Setting
Route::get('company-setting', 'CompanyController@manageCompany');

// route Approval
Route::get('approval', 'ApprovalController@index');


//salary_level_master
Route::get('/masterdata-payrollmaster-salarylevelmaster', 'PayrollMasterController@SalaryLevelMasterIndex');
Route::post('/masterdata-payrollmaster-salarylevelmaster-store', 'PayrollMasterController@SalaryLevelMasterStore');
Route::get('/masterdata-payrollmaster-salarylevelmaster-edit/{id}', 'PayrollMasterController@editSalaryLevel');
Route::post('/masterdata-payrollmaster-salarylevelmaster-edit/{id}', 'PayrollMasterController@doeditSalaryLevel');
Route::get('/masterdata-payrollmaster-salarylevelmaster-delete/{id}', 'PayrollMasterController@deleteSalaryLevel');
Route::post('/masterdata-payrollmaster-salarylevelmaster-search', 'PayrollMasterController@searchSalaryLevel');

// BPJS_Type_master
Route::get('/masterdata-payrollmaster-BPJStypemaster', 'PayrollMasterController@BPJSTypeMasterIndex');
Route::post('/masterdata-payrollmaster-BPJStypemaster-store', 'PayrollMasterController@BPJSTypeMasterStore');
Route::get('/masterdata-payrollmaster-BPJStypemaster-edit/{id}', 'PayrollMasterController@editBPJSType');
Route::post('/masterdata-payrollmaster-BPJStypemaster-edit/{id}', 'PayrollMasterController@doeditBPJSType');
Route::get('/masterdata-payrollmaster-BPJStypemaster-delete/{id}', 'PayrollMasterController@deleteBPJSType');
Route::post('/masterdata-payrollmaster-BPJStypemaster-search', 'PayrollMasterController@searchBPJSType');


// ini untuk leave type master
// ini view
Route::get('/masterdata-payrollmaster-leavetypemaster', 'PayrollMasterController@LeaveTypeMasterIndex');
// ini untuk add
Route::post('/masterdata-payrollmaster-leavetypemaster-store', 'PayrollMasterController@LeaveTypeMasterStore');
// delete
Route::get('/masterdata-payrollmaster-leavetypemaster-delete/{id}', 'PayrollMasterController@deleteLeaveType');
// edit
Route::get('/masterdata-payrollmaster-leavetypemaster-edit/{id}', 'PayrollMasterController@editLeaveType');
Route::post('/masterdata-payrollmaster-leavetypemaster-edit/{id}', 'PayrollMasterController@doeditLeaveType');
// end leave type


// ini untuk incentive master
Route::get('/masterdata-payrollmaster-incentivemaster', 'PayrollMasterController@IncentiveMasterIndex');
Route::post('/masterdata-payrollmaster-incentivemaster-store', 'PayrollMasterController@IncentiveMasterStore');
Route::get('/masterdata-payrollmaster-incentivemaster-delete/{id}', 'PayrollMasterController@deleteIncentive');
Route::get('/masterdata-payrollmaster-incentivemaster-edit/{id}', 'PayrollMasterController@editIncentive');
Route::post('/masterdata-payrollmaster-incentivemaster-edit/{id}', 'PayrollMasterController@doeditIncentive');
// end incentive

// ini untuk pph master
Route::get('/masterdata-payrollmaster-PPHtypemaster', 'PayrollMasterController@PPHtypeMasterIndex');
Route::post('/masterdata-payrollmaster-PPHtypemaster-store', 'PayrollMasterController@PPHTypeMasterStore');
Route::get('/masterdata-payrollmaster-PPHtypemaster-delete/{id}', 'PayrollMasterController@deletePPH');
Route::get('/masterdata-payrollmaster-PPHtypemaster-edit/{id}', 'PayrollMasterController@editPPH');
Route::post('/masterdata-payrollmaster-PPHtypemaster-edit/{id}', 'PayrollMasterController@doeditPPH');
// end pph

// ini untuk PTKP Master
Route::get('/masterdata-payrollmaster-PTKPtypemaster', 'PayrollMasterController@PTKPTypeMasterIndex');
Route::post('/masterdata-payrollmaster-PTKPtypemaster-store', 'PayrollMasterController@PTKPTypeMasterStore');
Route::get('/masterdata-payrollmaster-PTKPtypemaster-delete/{id}', 'PayrollMasterController@deletePTKP');
Route::get('/masterdata-payrollmaster-PTKPtypemaster-edit/{id}', 'PayrollMasterController@editPTKP');
Route::post('/masterdata-payrollmaster-PTKPtypemaster-edit/{id}', 'PayrollMasterController@doeditPTKP');
// end PTKP


// ini untuk Working days Master
Route::get('/masterdata-payrollmaster-workingdaysmaster', 'PayrollMasterController@WorkingDaysMasterIndex');
// end Working days Master

// ini untuk submission form leave
